const { Readable, Writable, Duplex } = require('stream');
const { assert } = require('chai');
const { suite, test } = require('mocha');
const http = require('http');
const net = require('net');
const path = require('path');
const HttpStream = require('../src/HttpStream');
const listenerHelper = require('./helper/listener');
const bufferHelper = require('./helper/buffer');
const streamHelper = require('./helper/stream');

suite('HttpStream', function () {
    suite('Readable', function () {
        test('1MB transfer, 2048B water mark', listenerHelper.protectTest(function ({ protect, whenDone, done }) {
            this.slow(200);
            this.timeout(5000);
            const source = bufferHelper.randomBuffer(1024 * 1024);
            const target = Buffer.alloc(source.length);
            let targetOffset = 0;
            const readable = streamHelper.fromBuffer(source, 1200);
            const waterMark = 2048;
            const stream = new HttpStream({
                readableHighWaterMark: waterMark
            });
            stream.setReadable(readable);
            protect(stream).on('data', data => {
                assert.strictEqual(data.length, waterMark, `data.length === ${waterMark}`);
                assert.isAtMost(targetOffset + data.length, target.length, `targetOffset + data.length <= ${target.length}`);
                data.copy(target, targetOffset);
                targetOffset += data.length;
            }).once('end', () => {
                assert.strictEqual(targetOffset, target.length, `targetOffset === ${target.length}`);
                bufferHelper.assertEqual(target, source);
                done();
            }).once('error', error => {
                done(error);
            });
            whenDone(() => {
                readable.destroy();
            });
        }));
        test('1MB transfer, 2048B water mark, error on 512KB', listenerHelper.protectTest(function ({ protect, whenDone, done }) {
            const source = bufferHelper.randomBuffer(1024 * 1024);
            const target = Buffer.alloc(source.length);
            let targetOffset = 0;
            const targetError = new Error('test');
            const targetMark = 512 * 1024;
            const readable = streamHelper.fromBufferWithError(source, targetMark, readable => {
                readable.destroy(targetError);
            }, 1200);
            const waterMark = 2048;
            const stream = new HttpStream({
                readableHighWaterMark: waterMark
            });
            stream.setReadable(readable);
            protect(stream).on('data', data => {
                assert.strictEqual(data.length, waterMark, `data.length === ${waterMark}`);
                assert.isAtMost(targetOffset + data.length, targetMark, `targetOffset + data.length <= ${targetMark}`);
                data.copy(target, targetOffset);
                targetOffset += data.length;
            }).once('end', () => {
                assert.strictEqual(targetOffset, targetMark, `targetOffset === ${targetMark}`);
                bufferHelper.assertEqual(target.slice(0, targetMark), source.slice(0, targetMark));
                assert.fail('Error from underlying readable not transmitted to HttpStream');
            }).once('error', error => {
                assert.strictEqual(error, targetError, `error === targetError`);
                done();
            });
            whenDone(() => {
                readable.destroy();
            });
        }));
        test('HTTP POST transfer', listenerHelper.protectTest(function ({ protect, whenDone, done }) {
            this.slow(1000);
            this.timeout(5000);
            this.timeout(0);
            const sendBuffer = bufferHelper.randomBuffer(1024 * 1024);
            const recvBuffer = bufferHelper.randomBuffer(1024 * 1024);
            const date = new Date();
            let sendReadOffset = 0;
            let recvReadOffset = 0;
            let serverRequest, serverResponse, clientRequest, clientResponse;
            const server = http.createServer((request, response) => {
                serverRequest = request;
                serverResponse = response;
                protect(request).on('data', data => {
                    assert.isAtMost(sendReadOffset + data.length, sendBuffer.length, `sendReadOffset + data.length <= ${sendBuffer.length}`);
                    bufferHelper.assertEqual(data, sendBuffer.slice(sendReadOffset, sendReadOffset + data.length));
                    sendReadOffset += data.length;
                }).once('end', () => {
                    assert.strictEqual(sendReadOffset, sendBuffer.length, `sendReadOffset === ${sendBuffer.length}`);
                    response.setHeader('Content-Length', recvBuffer.length.toString(10));
                    response.setHeader('Content-Type', 'application/octet-stream');
                    response.setHeader('Date', date.toUTCString());
                    response.setHeader('Server', path.basename(__filename));
                    response.end(recvBuffer);
                });
            }).listen(0, '127.0.0.1', () => {
                const port = server.address().port;
                clientRequest = http.request({
                    host: '127.0.0.1',
                    port,
                    method: 'POST',
                    path: '/'
                });
                clientRequest.setHeader('Content-Length', sendBuffer.length.toString(10));
                clientRequest.setHeader('Content-Type', 'application/octet-stream');
                clientRequest.setHeader('Date', date.toUTCString());
                clientRequest.setHeader('User-Agent', path.basename(__filename));
                protect(clientRequest).once('response', response => {
                    clientResponse = response;
                    stream.setReadable(response);
                });
                const stream = new HttpStream();
                stream.setWritable(clientRequest);
                protect(stream).on('data', data => {
                    assert.isAtMost(recvReadOffset + data.length, recvBuffer.length, `recvReadOffset + data.length <= ${recvBuffer.length}`);
                    bufferHelper.assertEqual(data, recvBuffer.slice(recvReadOffset, recvReadOffset + data.length));
                    recvReadOffset += data.length;
                }).once('end', () => {
                    assert.strictEqual(recvReadOffset, recvBuffer.length, `recvReadOffset === ${recvBuffer.length}`);
                    done();
                }).end(sendBuffer);
            });
            whenDone(() => {
                server.close();
            });
        }));
        test('HTTP POST transfer with server abort', listenerHelper.protectTest(function ({ protect, whenDone, done }) {
            this.slow(1000);
            this.timeout(5000);
            this.timeout(0);
            const sendBuffer = bufferHelper.randomBuffer(1024 * 1024);
            const recvBuffer = bufferHelper.randomBuffer(1024 * 1024);
            const date = new Date();
            let sendReadOffset = 0;
            let recvReadOffset = 0;
            let serverRequest, serverResponse, clientRequest, clientResponse;
            const server = http.createServer((request, response) => {
                serverRequest = request;
                serverResponse = response;
                protect(request).on('data', data => {
                    assert.isAtMost(sendReadOffset + data.length, sendBuffer.length, `sendReadOffset + data.length <= ${sendBuffer.length}`);
                    bufferHelper.assertEqual(data, sendBuffer.slice(sendReadOffset, sendReadOffset + data.length));
                    sendReadOffset += data.length;
                }).once('end', () => {
                    assert.strictEqual(sendReadOffset, sendBuffer.length, `sendReadOffset === ${sendBuffer.length}`);
                    response.setHeader('Content-Length', recvBuffer.length.toString(10));
                    response.setHeader('Content-Type', 'application/octet-stream');
                    response.setHeader('Date', date.toUTCString());
                    response.setHeader('Server', path.basename(__filename));
                    response.write(recvBuffer.slice(0, 512 * 1024), () => {
                        response.destroy();
                    });
                });
            }).listen(0, '127.0.0.1', () => {
                const port = server.address().port;
                clientRequest = http.request({
                    host: '127.0.0.1',
                    port,
                    method: 'POST',
                    path: '/'
                });
                clientRequest.setHeader('Content-Length', sendBuffer.length.toString(10));
                clientRequest.setHeader('Content-Type', 'application/octet-stream');
                clientRequest.setHeader('Date', date.toUTCString());
                clientRequest.setHeader('User-Agent', path.basename(__filename));
                protect(clientRequest).once('response', response => {
                    clientResponse = response;
                    stream.setReadable(response);
                });
                const stream = new HttpStream();
                stream.setWritable(clientRequest);
                protect(stream).on('data', data => {
                    assert.isAtMost(recvReadOffset + data.length, recvBuffer.length, `recvReadOffset + data.length <= ${recvBuffer.length}`);
                    bufferHelper.assertEqual(data, recvBuffer.slice(recvReadOffset, recvReadOffset + data.length));
                    recvReadOffset += data.length;
                }).once('end', () => {
                    assert.fail('Event "end" called, while expecting an "error" event');
                }).once('error', error => {
                    assert.strictEqual(recvReadOffset, 512 * 1024, `recvReadOffset === ${recvBuffer.length}`);
                    assert(error.code === 'ECONNRESET', `error.code === 'ECONNRESET'`);
                    done();
                }).end(sendBuffer);
            });
            whenDone(() => {
                server.close();
            });
        }));
        test('HTTP POST transfer with client abort', listenerHelper.protectTest(function ({ protect, whenDone, done }) {
            this.slow(1000);
            this.timeout(5000);
            this.timeout(0);
            const sendBuffer = bufferHelper.randomBuffer(1024 * 1024);
            const recvBuffer = bufferHelper.randomBuffer(1024 * 1024);
            const date = new Date();
            let sendReadOffset = 0;
            let recvReadOffset = 0;
            let serverRequest, serverResponse, clientRequest, clientResponse;
            const server = http.createServer((request, response) => {
                serverRequest = request;
                serverResponse = response;
                protect(request).on('data', data => {
                    assert.isAtMost(sendReadOffset + data.length, sendBuffer.length, `sendReadOffset + data.length <= ${sendBuffer.length}`);
                    bufferHelper.assertEqual(data, sendBuffer.slice(sendReadOffset, sendReadOffset + data.length));
                    sendReadOffset += data.length;
                }).once('end', () => {
                    assert.strictEqual(sendReadOffset, sendBuffer.length, `sendReadOffset === ${sendBuffer.length}`);
                    response.setHeader('Content-Length', recvBuffer.length.toString(10));
                    response.setHeader('Content-Type', 'application/octet-stream');
                    response.setHeader('Date', date.toUTCString());
                    response.setHeader('Server', path.basename(__filename));
                    response.end(recvBuffer);
                });
            }).listen(0, '127.0.0.1', () => {
                const port = server.address().port;
                clientRequest = http.request({
                    host: '127.0.0.1',
                    port,
                    method: 'POST',
                    path: '/'
                });
                clientRequest.setHeader('Content-Length', sendBuffer.length.toString(10));
                clientRequest.setHeader('Content-Type', 'application/octet-stream');
                clientRequest.setHeader('Date', date.toUTCString());
                clientRequest.setHeader('User-Agent', path.basename(__filename));
                protect(clientRequest).once('response', response => {
                    clientResponse = response;
                    stream.setReadable(response);
                });
                const stream = new HttpStream();
                stream.setWritable(clientRequest);
                protect(stream).on('data', data => {
                    assert.isAtMost(recvReadOffset + data.length, recvBuffer.length, `recvReadOffset + data.length <= ${recvBuffer.length}`);
                    bufferHelper.assertEqual(data, recvBuffer.slice(recvReadOffset, recvReadOffset + data.length));
                    recvReadOffset += data.length;
                }).once('end', () => {
                    assert.fail('Event "end" called, while expecting an "error" event');
                }).once('error', error => {
                    assert.strictEqual(recvReadOffset, 0, `recvReadOffset === 0`);
                    assert.strictEqual(error.code, 'ECONNRESET', `error.code === 'ECONNRESET'`);
                    done();
                }).write(sendBuffer.slice(0, 512 * 1024), () => {
                    clientRequest.abort();
                });
            });
            whenDone(() => {
                server.close();
            });
        }));
    });
});
