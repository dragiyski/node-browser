(function () {
    'use strict';

    exports.protectListeners = function (done) {
        const objects = new Map();
        const whenDone = new Set();
        return {
            protect(emitter) {
                const hookAdd = Object.create(null);
                const hookRemove = Object.create(null);
                for (const hookName of ['addListener', 'on', 'once', 'prependListener', 'prependOnceListener']) {
                    hookAdd[hookName] = function (event, listener) {
                        const protectedListener = addListener(emitter, event, listener);
                        emitter[hookName](event, protectedListener);
                        return this;
                    };
                }
                for (const hookName of ['removeListener', 'off']) {
                    hookRemove[hookName] = function (event, listener) {
                        const protectedListener = getListener(emitter, event, listener);
                        if (protectedListener != null) {
                            emitter[hookName](event, protectedListener);
                        } else {
                            emitter[hookName](event, listener);
                        }
                        return this;
                    };
                }
                return new Proxy(emitter, {
                    get(target, property, receiver) {
                        if (property in hookAdd) {
                            return hookAdd[property];
                        }
                        if (property in hookRemove) {
                            return hookRemove[property];
                        }
                        return Reflect.get(target, property, receiver);
                    }
                });
            },
            whenDone(callback) {
                whenDone.add(callback);
            },
            done(error) {
                clear();
                done(error);
            }
        };

        function protect(listener) {
            return function () {
                try {
                    return listener.apply(this, arguments);
                } catch (e) {
                    clear();
                    done(e);
                }
            };
        }

        function clear() {
            for (const [object, eventMap] of objects) {
                for (const event in eventMap) {
                    for (const [, protectedListener] of eventMap[event]) {
                        object.removeListener(event, protectedListener);
                    }
                }
            }
            for (const callback of whenDone) {
                callback();
            }
        }

        function addListener(target, event, listener) {
            if (!objects.has(target)) {
                objects.set(target, Object.create(null));
            }
            const eventMap = objects.get(target);
            if (!(event in eventMap)) {
                eventMap[event] = new Map();
            }
            const protectionMap = eventMap[event];
            if (!protectionMap.has(listener)) {
                protectionMap.set(listener, protect(listener));
            }
            return protectionMap.get(listener);
        }

        function getListener(target, event, listener) {
            if (objects.has(target)) {
                const eventMap = objects.get(target);
                if (event in eventMap) {
                    const protectionMap = eventMap[event];
                    if (protectionMap.has(listener)) {
                        return protectionMap.get(listener);
                    }
                }
            }
        }
    };

    exports.protectTest = function (callback) {
        return function (done) {
            try {
                return callback.call(this, exports.protectListeners(done));
            } catch (e) {
                done(e);
            }
        };
    };
})();
