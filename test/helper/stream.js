(function () {
    const { Readable, Writable, Duplex } = require('stream');
    const { performance } = require('perf_hooks');
    const bufferHelper = require('./buffer');

    exports.fromBuffer = function (buffer, highWaterMark = 65536) {
        let offset = 0;
        return new Readable({
            highWaterMark,
            encoding: null,
            objectMode: false,
            emitClose: true,
            autoDestroy: true,
            read(size) {
                while (size > 0 && offset < buffer.length) {
                    const readSize = Math.min(size, buffer.length - offset);
                    const chunk = buffer.slice(offset, offset + readSize);
                    offset += readSize;
                    size -= readSize;
                    if (!this.push(chunk)) {
                        break;
                    }
                }
                if (offset >= buffer.length) {
                    this.push(null);
                }
            }
        });
    };
    exports.fromBufferWithError = function (buffer, mark, markCallback, highWaterMark = 65536) {
        let offset = 0;
        if (mark < 0 || mark >= buffer.length) {
            throw new RangeError(`Invalid mark range, expected: 0 <= mark < ${buffer.length}, got ${mark}`);
        }
        return new Readable({
            highWaterMark,
            encoding: null,
            objectMode: false,
            emitClose: true,
            autoDestroy: true,
            read(size) {
                while (size > 0 && offset < mark) {
                    const readSize = Math.min(size, mark - offset);
                    const chunk = buffer.slice(offset, offset + readSize);
                    offset += readSize;
                    size -= readSize;
                    if (!this.push(chunk)) {
                        break;
                    }
                }
                if (offset >= mark) {
                    markCallback(this);
                }
            }
        });
    };
    exports.randomDataAtSpeed = function ({ stream, length, blockSize, bytesPerSecond }, callback) {
        let lastWrite = null;
        let prevWrite = null;
        let writeTimer = null;
        let written = 0;
        const blocksPerSecond = bytesPerSecond / blockSize;
        const blockPeriodMs = 1000 / blocksPerSecond;

        return initial;

        function nextWrite(error) {
            if (error) {
                return void callback(error);
            }
            prevWrite = lastWrite;
            lastWrite = {
                written,
                time: performance.timeOrigin + performance.now()
            };
            if (writeTimer != null) {
                clearTimeout(writeTimer);
                writeTimer = null;
            }
            if (written < length) {
                if (prevWrite != null) {
                    const duration = lastWrite.time - prevWrite.time;
                    if (blockPeriodMs - duration >= 1) {
                        writeTimer = setTimeout(write, blockPeriodMs - duration);
                        return;
                    }
                }
                write();
            } else {
                stream.end(callback);
            }
        }

        function write() {
            if (writeTimer != null) {
                clearTimeout(writeTimer);
                writeTimer = null;
            }
            if (stream.writable) {
                const bytesToWrite = Math.min(blockSize, length - written);
                const buffer = bufferHelper.randomBuffer(bytesToWrite);
                written += buffer.length;
                stream.write(buffer, nextWrite);
            }
        }

        function initial() {
            lastWrite = {
                written,
                time: performance.timeOrigin + performance.now()
            };
            write();
        }
    };

    exports.dataAtSpeed = function ({ stream, buffer, blockSize, bytesPerSecond }, callback) {
        let lastWrite = null;
        let prevWrite = null;
        let writeTimer = null;
        let written = 0;
        const blocksPerSecond = bytesPerSecond / blockSize;
        const blockPeriodMs = 1000 / blocksPerSecond;

        return initial;

        function nextWrite(error) {
            if (error) {
                return void callback(error);
            }
            prevWrite = lastWrite;
            lastWrite = {
                written,
                time: performance.timeOrigin + performance.now()
            };
            if (writeTimer != null) {
                clearTimeout(writeTimer);
                writeTimer = null;
            }
            if (written < buffer.length) {
                if (prevWrite != null) {
                    const duration = lastWrite.time - prevWrite.time;
                    if (blockPeriodMs - duration >= 1) {
                        writeTimer = setTimeout(write, blockPeriodMs - duration);
                        return;
                    }
                }
                write();
            } else {
                stream.end(callback);
            }
        }

        function write() {
            if (writeTimer != null) {
                clearTimeout(writeTimer);
                writeTimer = null;
            }
            if (stream.writable) {
                const bytesToWrite = Math.min(blockSize, buffer.length - written);
                stream.write(buffer.slice(written, written + bytesToWrite), nextWrite);
                written += bytesToWrite;
            }
        }

        function initial() {
            lastWrite = {
                written,
                time: performance.timeOrigin + performance.now()
            };
            write();
        }
    };
})();
