(function () {
    'use strict';

    const childProcess = require('child_process');
    const path = require('path');

    let hasInspect = false;
    let hasInspectBrk = false;
    let disableInspect = false;
    const inspectPorts = new Set();

    for (const arg of process.execArgv) {
        if (arg === '--inspect' || arg === '--inspect-brk' || arg.startsWith('--inspect=') || arg.startsWith('--inspect-brk=')) {
            hasInspect = true;
        }
        if (arg === '--inspect-brk' || arg.startsWith('--inspect-brk=')) {
            hasInspectBrk = true;
        }
    }

    for (const arg of process.argv) {
        if (arg === '--no-child-inspect') {
            disableInspect = true;
        }
    }

    exports.http = function () {
        return makeServerProcess(path.resolve(__dirname, 'server/http'));
    };

    function makeServerProcess(module) {
        const options = {
            serialization: 'advanced',
            stdio: ['inherit', 'inherit', 'inherit', 'ipc']
        };
        let port = null;
        if (hasInspect) {
            // 9229 is the default inspect port
            port = 9230;
            for (; port < 65535; ++port) {
                if (!inspectPorts.has(port)) {
                    break;
                }
            }
            if (port >= 65536) {
                port = null;
            }
            if (port != null) {
                if (hasInspectBrk) {
                    options.execArgv = [`--inspect-brk=${port}`];
                } else {
                    options.execArgv = [`--inspect=${port}`];
                }
            }
        }
        if (disableInspect) {
            options.execArgv = [];
        }
        const serverProcess = childProcess.fork(module, options);
        serverProcess.once('exit', () => {
            if (port != null) {
                inspectPorts.delete(port);
            }
        });
        return serverProcess;
    }
})();
