(function () {
    'use strict';

    const { assert } = require('chai');

    exports.randomBuffer = function (length) {
        const buffer = Buffer.allocUnsafe(length);
        for (let i = 0; i < length; ++i) {
            buffer[i] = Math.floor(Math.random() * 256);
        }
        return buffer;
    };
    exports.assertEqual = function (target, source) {
        assert.strictEqual(target.length, source.length, `a.length === b.length`);
        for (let i = 0; i < target.length; ++i) {
            if (target[i] !== source[i]) {
                assert.fail(`target[${i}] === source[${i}]`);
            }
        }
    };
})();
