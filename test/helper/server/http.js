#!/usr/bin/env node
(function () {
    const http = require('http');
    const streamHelper = require('../stream');
    const bufferHelper = require('../buffer');

    if (typeof process.send !== 'function') {
        console.error(`Process ${process.pid} is not a child process of a NodeJS process.\nModule ${module.id} can only be executed with childProcess.fork() or childProcess.spawn()`);
        process.exit(1);
    }

    let settings = null;

    const messageReceiver = {
        start: function () {
            const server = http.createServer(serverRequest).listen(0, '127.0.0.1', () => {
                process.send({
                    type: 'listen',
                    port: server.address().port
                });
            });
        },
        settings: function (message) {
            delete message.type;
            settings = message;
            process.send({
                type: 'ready'
            });
        },
        exit: function ({ code }) {
            process.exit(code);
        }
    };

    process.on('message', message => {
        if (typeof message.type === 'string' && Object.hasOwnProperty.call(messageReceiver, message.type) && typeof messageReceiver[message.type] === 'function') {
            messageReceiver[message.type](message);
        }
    }).send({ type: 'ready' });

    function serverRequest(request, response) {
        const { length, blockSize, bytesPerSecond } = settings;
        let { respondAfter } = settings;
        const buffer = bufferHelper.randomBuffer(length);
        process.send({
            type: 'connection',
            buffer,
            headers: request.rawHeaders,
            id: 1
        });
        messageReceiver.ready = () => {
            if (request.method === 'GET' || request.method === 'HEAD') {
                respondAfter = false;
            } else if (respondAfter == null) {
                respondAfter = true;
            }
            response.setHeader('Content-Length', length);
            response.setHeader('Content-Type', 'application/octet-stream');
            const send = streamHelper.dataAtSpeed({
                stream: response,
                buffer,
                blockSize,
                bytesPerSecond
            }, error => {
                if (error) {
                    console.error(error);
                    process.exit(1);
                }
            });
            request.on('data', data => {
                process.send({
                    type: 'recv',
                    data,
                    id: 1
                });
            }).once('end', () => {
                if (respondAfter === true) {
                    send();
                }
                process.send({
                    type: 'request'
                });
            });
            if (respondAfter === false) {
                send();
            } else if (typeof respondAfter === 'number') {
                setTimeout(send, respondAfter);
            }
        };
    }
})();
