(function () {
    'use strict';
    const { assert } = require('chai');
    const listenerHelper = require('./listener');
    const serverHelper = require('./server');
    const streamHelper = require('./stream');
    const bufferHelper = require('./buffer');
    const formatUrl = require('url').format;
    const HttpTransport = require('../../src/HttpTransport');

    // const { EventEmitter } = require('events');
    // // eslint-disable-next-line camelcase
    // const EventEmitter_emit = EventEmitter.prototype.emit;
    // EventEmitter.prototype.emit = function (event) {
    //     printEventCall(`${this.constructor.name}::${event}`);
    //     return EventEmitter_emit.apply(this, arguments);
    // };
    //
    // function printEventCall(text) {
    //     console.log(`event: ${text}`);
    // }

    exports.test = function (options) {
        return listenerHelper.protectTest(function ({ protect, whenDone, done }) {
            debugger;
            if (typeof options.slow === 'number') {
                this.slow(options.slow);
            }
            if (typeof options.timeout === 'number') {
                this.timeout(options.timeout);
            }
            if (typeof options.initialize === 'function') {
                const { initialize } = options;
                initialize(protect, done);
            }
            let transport = null;
            const contextMap = new Map();
            const messageQueue = [{ type: 'settings', ...options.recv }, { type: 'start' }];
            const serverProcess = serverHelper.http();
            let expectSignal = null;
            protect(serverProcess).on('message', onServerMessage).once('error', onServerError).once('exit', onServerExit);

            function onServerMessage({ type, ...attributes }) {
                switch (type) {
                    case 'ready':
                        if (messageQueue.length > 0) {
                            serverProcess.send(messageQueue.shift());
                        }
                        break;
                    case 'listen':
                        assert.strictEqual(typeof attributes.port, 'number', `typeof attributes.port === 'number'`);
                        assert.isAbove(attributes.port, 0, `attributes.port > 0`);
                        assert.isAtMost(attributes.port, 65535, `attributes.port <= 65535`);
                        createTransport(attributes.port);
                        break;
                    case 'connection':
                        processServerConnection(attributes);
                        break;
                    case 'recv':
                        reportSend(attributes);
                        break;
                    case 'request':
                        break;
                }
            }

            function onServerError(error) {
                done(error);
            }

            function onServerExit(code, signal) {
                if (expectSignal != null && signal === expectSignal) {
                    return;
                }
                if (signal) {
                    done(new Error(`Server exited with signal: ${signal}`));
                } else if (code !== 0) {
                    done(new Error(`Server exited with code: ${code}`));
                } else {
                    workDone();
                }
            }

            function onTransportError(error) {
                const { onTransportError } = options;
                if (typeof onTransportError === 'function') {
                    onTransportError(this, error);
                } else {
                    process.stdout.write(outputGeneral('* -- error'));
                    console.error(error.stack);
                }
            }

            function onTransportIdle() {
                this.destroy();
            }

            function onTransportClose() {
                const { onTransportClose } = options;
                if (typeof onTransportClose === 'function') {
                    onTransportClose(this, workDone);
                } else {
                    process.stdout.write(outputGeneral(`* -- close`));
                    workDone();
                }
            }

            function onStreamError(error) {
                const { onStreamError } = options;
                if (typeof onStreamError === 'function') {
                    onStreamError(this, error);
                } else {
                    process.stdout.write(outputGeneral(`${this.id} -- error`));
                    console.error(error.stack);
                    assert(contextMap.has(this.id));
                    const context = contextMap.get(this.id);
                    if (context.expectedError != null) {
                        let errorMatch = true;
                        if ('code' in context.expectedError) {
                            if (error.code !== context.expectedError.code) {
                                errorMatch = false;
                            }
                        }
                        if ('message' in context.expectedError) {
                            if (error.message !== context.expectedError.message) {
                                errorMatch = false;
                            }
                        }
                        if ('constructor' in context.expectedError) {
                            if (!(error instanceof context.expectedError.constructor)) {
                                errorMatch = false;
                            }
                        }
                        if (errorMatch) {
                            context.lastError = error;
                            context.expectedClose = true;
                            return;
                        }
                    }
                    done(error);
                }
            }

            function onStreamAbort() {
                assert(contextMap.has(this.id));
                const context = contextMap.get(this.id);
                context.expectedClose = true;
                const { onStreamAbort } = options;
                if (typeof onStreamAbort === 'function') {
                    onStreamAbort(this);
                } else {
                    process.stdout.write(outputGeneral(`${this.id} -- aborted`));
                    if (!context.expectedAbort) {
                        done(new Error('Unexpected request abort'));
                    }
                }
            }

            function onStreamResponse() {
                process.stdout.write(outputRecv(`${this.id} => status: HTTP/${this.httpVersion} ${this.statusCode} ${this.statusMessage}`, true));
                for (let i = 0; i < this.responseRawHeaders.length; i += 2) {
                    process.stdout.write(outputRecv(`${this.id} => header: ${this.responseRawHeaders[i]}: ${this.responseRawHeaders[i + 1]}`, true));
                }
                const context = contextMap.get(this.id);
                assert(context != null && typeof context === 'object', `context is object`);
                assert.strictEqual(this.statusCode, 200, `this.statusCode === 200`);
                assert('content-length' in this.responseHeaders, `'content-length' in stream.responseHeaders`);
                assert(/[0-9]+/.test(this.responseHeaders['content-length']), `stream.responseHeaders['content-length'] is a numeric`);
                const recvLength = parseInt(this.responseHeaders['content-length']);
                assert(isFinite(recvLength), `isFinite(length)`);
                assert.strictEqual(recvLength, context.recvExpect.length, `Content-Length is ${context.recvExpect.length}`);
            }

            function onStreamData(data) {
                const stream = this;
                const context = contextMap.get(stream.id);
                assert.isAtMost(context.recvOffset + data.length, context.recvExpect.length, `recv <= ${context.recvExpect.length}`);
                data.copy(context.recvActual, context.recvOffset);
                context.recvOffset += data.length;
                if (context.abort != null && isFinite(context.abort.after) && context.recvOffset >= context.abort.after && !context.aborted) {
                    context.aborted = true;
                    if (context.abort.type === 'server') {
                        context.expectedError = {
                            code: 'ECONNRESET'
                        };
                        process.stdout.write(outputGeneral(`${context.stream.id} -- abort: server`));
                        expectSignal = 'SIGKILL';
                        serverProcess.kill('SIGKILL');
                    } else if (context.abort.type === 'client') {
                        process.stdout.write(outputGeneral(`${context.stream.id} -- abort: client`));
                        context.expectedAbort = true;
                        stream.abort();
                    }
                }
                reportRecv(data.length, context);
            }

            function onStreamEnd() {
                assert(contextMap.has(this.id));
                const context = contextMap.get(this.id);
                assert.strictEqual(context.recvOffset, context.recvExpect.length, `recv === ${context.recvExpect.length}`);
                bufferHelper.assertEqual(context.recvExpect, context.recvActual);
                const { onStreamEnd } = options;
                if (typeof onStreamEnd === 'function') {
                    onStreamEnd(this);
                } else {
                    process.stdout.write(outputGeneral(`${this.id} -- end`));
                    assert(contextMap.has(this.id));
                    const context = contextMap.get(this.id);
                    context.expectedClose = true;
                }
            }

            function onStreamClose() {
                const { onStreamClose } = options;
                if (typeof onStreamClose === 'function') {
                    onStreamClose(this, workDone);
                } else {
                    process.stdout.write(outputGeneral(`${this.id} -- close`));
                    assert(contextMap.has(this.id));
                    const context = contextMap.get(this.id);
                    if (!context.expectedClose) {
                        done(new Error('Unexpected close of stream'));
                    } else {
                        workDone();
                    }
                }
            }

            function createTransport(port) {
                process.stdout.write(outputGeneral(`* -- transport: ${port}`));
                transport = new HttpTransport(formatUrl({
                    protocol: 'http:',
                    slashes: true,
                    hostname: 'play.dragiyski.org',
                    port
                }));
                if (typeof options.onTransport === 'function') {
                    const { onTransport } = options;
                    onTransport(transport);
                }
                protect(transport)
                    .on('available', nextStream)
                    .on('lookup', onLookup)
                    .once('error', onTransportError)
                    .once('idle', onTransportIdle)
                    .once('close', onTransportClose);
                const { onTransport } = options;
                if (typeof onTransport === 'function') {
                    onTransport(transport);
                }
            }

            function processServerConnection({ id, buffer, headers }) {
                assert.strictEqual(typeof id, 'number', `typeof id === 'number'`);
                assert(Buffer.isBuffer(buffer), `Buffer.isBuffer(buffer)`);
                if (!contextMap.has(id)) {
                    contextMap.set(id, {});
                }
                const context = contextMap.get(id);
                context.recvExpect = buffer;
                assert(Array.isArray(headers) && headers.length % 2 === 0, `headers must be an array of even number of elements`);
                for (let i = 0; i < headers.length; i += 2) {
                    process.stdout.write(outputSend(`${context.stream.id} <= header: ${headers[i]}: ${headers[i + 1]}`, true));
                }
                serverProcess.send({
                    type: 'ready'
                });
            }

            function nextStream() {
                const { requestQueue } = options;
                if (Array.isArray(requestQueue) && requestQueue.length > 0) {
                    const request = requestQueue.shift();
                    assert(request != null && typeof request === 'object', `request is object`);
                    const streamOptions = {};
                    if (typeof request.method === 'string') {
                        streamOptions.method = request.method;
                    } else {
                        streamOptions.method = 'GET';
                    }
                    if (typeof request.url === 'string') {
                        streamOptions.url = request.url;
                    } else {
                        streamOptions.url = '/';
                    }
                    if (request.headers != null && typeof request.headers === 'object') {
                        streamOptions.headers = request.headers;
                    } else {
                        streamOptions.headers = {};
                    }
                    const stream = this.request(streamOptions);
                    assert.strictEqual(typeof stream.id, 'number', `typeof stream.id === 'number'`);
                    if (!contextMap.has(stream.id)) {
                        contextMap.set(stream.id, {});
                    }
                    if (typeof options.onStream === 'function') {
                        const { onStream } = options;
                        onStream(stream);
                    }
                    protect(stream)
                        .on('data', onStreamData)
                        .on('error', onStreamError)
                        .once('end', onStreamEnd)
                        .once('abort', onStreamAbort)
                        .once('close', onStreamClose)
                        .once('response', onStreamResponse);
                    const context = contextMap.get(stream.id);
                    context.stream = stream;
                    context.recvActual = Buffer.alloc(options.recv.length);
                    context.recvOffset = 0;
                    context.sendOffset = 0;
                    if (request.abort != null) {
                        context.abort = request.abort;
                    }
                    if (request.send != null) {
                        context.send = request.send;
                    }
                    if (context.send != null && isFinite(context.send.length) && context.send.length > 0) {
                        assert(isFinite(context.send.blockSize) && context.send.blockSize > 0, `send.blockSize is positive number`);
                        assert(isFinite(context.send.bytesPerSecond) && context.send.bytesPerSecond > 0, `send.bytesPerSecond is positive number`);
                        context.sendExpect = bufferHelper.randomBuffer(context.send.length);
                        context.sendActual = Buffer.alloc(context.send.length);
                        streamHelper.dataAtSpeed({
                            stream,
                            buffer: context.sendExpect,
                            blockSize: context.send.blockSize,
                            bytesPerSecond: context.send.bytesPerSecond
                        })();
                    } else {
                        context.sendExpect = Buffer.alloc(0);
                        context.sendActual = Buffer.alloc(0);
                        stream.end();
                    }
                }
            }

            function reportSend({ id, data }) {
                assert(contextMap.has(id), `contextMap.has(id)`);
                const context = contextMap.get(id);
                assert.isAtMost(context.sendOffset + data.length, context.sendExpect.length, `context.sendOffset + data.length <= ${context.sendExpect.length}`);
                data.copy(context.sendActual, context.sendOffset);
                context.sendOffset += data.length;
                reportData(id, '<=', 'data', data.length, context.sendOffset, context.sendExpect.length);
            }

            function reportRecv(length, context) {
                reportData(context.stream.id, '=>', 'data', length, context.recvOffset, context.recvExpect.length);
            }

            function reportData(id, type, keyword, length, offset, expectLength) {
                const completed = (offset / expectLength) * 100;
                const message = `${id} ${type} ${keyword}: ${length} | ${offset} / ${expectLength} = ${completed.toFixed(2)}`;
                process.stdout.write(outputType(type)(message));
            }

            function workDone(error) {
                if (error) {
                    return void done(error);
                }
                if (transport != null && !transport.destroyed) {
                    return;
                }
                for (const [, context] of contextMap) {
                    if (context.stream != null && !context.stream.destroyed) {
                        return;
                    }
                }
                if (expectSignal == null && serverProcess.connected) {
                    serverProcess.send({
                        type: 'exit',
                        code: 0
                    });
                    return;
                }
                done();
            }

            whenDone(() => {
                for (const [id, context] of contextMap) {
                    if (context.stream != null) {
                        protect(context.stream)
                            .off('data', onStreamData)
                            .off('error', onStreamError)
                            .off('abort', onStreamAbort)
                            .off('close', onStreamClose)
                            .off('response', onStreamResponse);
                        if (!context.stream.destroyed) {
                            process.stdout.write(outputGeneral(`${id} -- destroy`));
                            context.stream.destroy();
                        }
                    }
                }
                if (transport != null) {
                    protect(transport)
                        .off('available', nextStream)
                        .off('lookup', onLookup)
                        .off('error', onTransportError)
                        .off('idle', onTransportIdle)
                        .off('close', onTransportClose);
                    if (!transport.destroyed) {
                        process.stdout.write(outputGeneral(`* -- destroy`));
                        transport.destroy();
                    }
                }
                if (serverProcess != null) {
                    protect(serverProcess).off('message', onServerMessage).off('error', onServerError).off('exit', onServerExit);
                    if (serverProcess.exitCode == null) {
                        serverProcess.kill('SIGKILL');
                    }
                }
            });
        });
    };

    function outputRecv(text, light = false) {
        if (process.stdout.hasColors()) {
            return `\x1b[${light ? '92' : '32'}m` + text + '\x1b[0m' + '\n';
        } else {
            return text + '\n';
        }
    }

    function outputSend(text, light = false) {
        if (process.stdout.hasColors()) {
            return `\x1b[${light ? '91' : '31'}m` + text + '\x1b[0m' + '\n';
        } else {
            return text + '\n';
        }
    }

    function outputGeneral(text) {
        if (process.stdout.hasColors()) {
            return '\x1b[37m' + text + '\x1b[0m' + '\n';
        } else {
            return text + '\n';
        }
    }

    function outputType(type) {
        switch (type) {
            case '<=':
                return outputSend;
            case '=>':
                return outputRecv;
            default:
                return outputGeneral;
        }
    }

    function onLookup(lookup) {
        lookup.host = '127.0.0.1';
    }
})();
