const { assert } = require('chai');
const { suite, test } = require('mocha');
const formatUrl = require('url').format;
const listenerHelper = require('./helper/listener');
const bufferHelper = require('./helper/buffer');
const streamHelper = require('./helper/stream');
const serverHelper = require('./helper/server');
const transferHelper = require('./helper/transfer');
const HttpTransport = require('../src/HttpTransport');

Error.stackTraceLimit = Infinity;

suite('HttpTransport', function () {
    test('GET HTTP/1.1, Clear-Text; Receive 1MiB, 500KiB/s; complete', transferHelper.test({
        server: 'http',
        slow: 2200,
        timeout: 0,
        recv: {
            length: 1024 * 1024,
            blockSize: 32000,
            bytesPerSecond: 500 * 1024
        },
        requestQueue: [
            {
                method: 'GET',
                headers: {
                    'Accept': '*/*',
                    'User-Agent': `NodeJS/${process.version}`
                }
            }
        ]
    }));
    test('GET HTTP/1.1, Clear-Text; Receive 1MiB, 500KiB/s; server abort (~300KiB)', transferHelper.test({
        server: 'http',
        slow: 800,
        timeout: 0,
        recv: {
            length: 1024 * 1024,
            blockSize: 32000,
            bytesPerSecond: 500 * 1024
        },
        requestQueue: [
            {
                method: 'GET',
                headers: {
                    'Accept': '*/*',
                    'User-Agent': `NodeJS/${process.version}`
                },
                abort: {
                    type: 'server',
                    after: 300 * 1024
                }
            }
        ]
    }));
    test('GET HTTP/1.1, Clear-Text; Receive 1MiB, 500KiB/s; client abort (~300KiB)', transferHelper.test({
        server: 'http',
        slow: 800,
        timeout: 0,
        recv: {
            length: 1024 * 1024,
            blockSize: 32000,
            bytesPerSecond: 500 * 1024
        },
        requestQueue: [
            {
                method: 'GET',
                headers: {
                    'Accept': '*/*',
                    'User-Agent': `NodeJS/${process.version}`
                },
                abort: {
                    type: 'client',
                    after: 300 * 1024
                }
            }
        ]
    }));
    test('GET HTTP/1.1, Clear-Text; Receive 3 * 1MiB, 500KiB/s; complete', transferHelper.test({
        server: 'http',
        slow: 6200,
        timeout: 0,
        recv: {
            length: 1024 * 1024,
            blockSize: 32000,
            bytesPerSecond: 500 * 1024
        },
        requestQueue: [
            {
                method: 'GET',
                headers: {
                    'Accept': '*/*',
                    'User-Agent': `NodeJS/${process.version}`
                }
            },
            {
                method: 'GET',
                headers: {
                    'Accept': '*/*',
                    'User-Agent': `NodeJS/${process.version}`
                }
            },
            {
                method: 'GET',
                headers: {
                    'Accept': '*/*',
                    'User-Agent': `NodeJS/${process.version}`
                }
            }
        ]
    }));
    test('POST HTTP/1.1, Clear-Text, Wait for Data; Send 1MiB, 300KiB/s; Receive 1MiB, 500KiB/s; complete', transferHelper.test({
        server: 'http',
        slow: 6200,
        timeout: 0,
        recv: {
            length: 1024 * 1024,
            blockSize: 32000,
            bytesPerSecond: 500 * 1024,
            respondAfter: true
        },
        requestQueue: [
            {
                method: 'POST',
                headers: {
                    'Accept': '*/*',
                    'User-Agent': `NodeJS/${process.version}`,
                    'Content-Type': 'application/octet-stream',
                    'Content-Length': 1024 * 1024
                },
                send: {
                    length: 1024 * 1024,
                    blockSize: 32768,
                    bytesPerSecond: 300 * 1024
                }
            }
        ]
    }));
    test('POST HTTP/1.1, Clear-Text, Interleaved; Send 1MiB, 300KiB/s; Receive 1MiB, 500KiB/s; complete', transferHelper.test({
        server: 'http',
        slow: 6200,
        timeout: 0,
        recv: {
            length: 1024 * 1024,
            blockSize: 32000,
            bytesPerSecond: 500 * 1024,
            respondAfter: false
        },
        requestQueue: [
            {
                method: 'POST',
                headers: {
                    'Accept': '*/*',
                    'User-Agent': `NodeJS/${process.version}`,
                    'Content-Type': 'application/octet-stream',
                    'Content-Length': 1024 * 1024
                },
                send: {
                    length: 1024 * 1024,
                    blockSize: 32768,
                    bytesPerSecond: 300 * 1024
                }
            }
        ]
    }));
    test('POST HTTP/1.1, Clear-Text, Wait 1000ms; Send 1MiB, 300KiB/s; Receive 1MiB, 500KiB/s; complete', transferHelper.test({
        server: 'http',
        slow: 6200,
        timeout: 0,
        recv: {
            length: 1024 * 1024,
            blockSize: 32000,
            bytesPerSecond: 500 * 1024,
            respondAfter: 1000
        },
        requestQueue: [
            {
                method: 'POST',
                headers: {
                    'Accept': '*/*',
                    'User-Agent': `NodeJS/${process.version}`,
                    'Content-Type': 'application/octet-stream',
                    'Content-Length': 1024 * 1024
                },
                send: {
                    length: 1024 * 1024,
                    blockSize: 32768,
                    bytesPerSecond: 300 * 1024
                }
            }
        ]
    }));
});
