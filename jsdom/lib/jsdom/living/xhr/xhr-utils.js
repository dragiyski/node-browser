'use strict';
const { EventEmitter } = require('events');
const { URL } = require('whatwg-url');
const DOMException = require('domexception/webidl2js-wrapper');
const utils = require('../../../../../src/utils');
const zlib = require('zlib');

const ProgressEvent = require('../generated/ProgressEvent');

const { fireAnEvent } = require('../helpers/events');

const headerListSeparatorRegexp = /,[ \t]*/;
const simpleMethods = new Set(['GET', 'HEAD', 'POST']);
const simpleHeaders = new Set(['accept', 'accept-language', 'content-language', 'content-type']);
const preflightHeaders = new Set([
    'access-control-expose-headers',
    'access-control-allow-headers',
    'access-control-allow-credentials',
    'access-control-allow-origin'
]);

const READY_STATES = exports.READY_STATES = Object.freeze({
    UNSENT: 0,
    OPENED: 1,
    HEADERS_RECEIVED: 2,
    LOADING: 3,
    DONE: 4
});

function getRequestHeader(requestHeaders, header) {
    const lcHeader = header.toLowerCase();
    const keys = Object.keys(requestHeaders);
    let n = keys.length;
    while (n--) {
        const key = keys[n];
        if (key.toLowerCase() === lcHeader) {
            return requestHeaders[key];
        }
    }
    return null;
}

function updateRequestHeader(requestHeaders, header, newValue) {
    const lcHeader = header.toLowerCase();
    const keys = Object.keys(requestHeaders);
    let n = keys.length;
    while (n--) {
        const key = keys[n];
        if (key.toLowerCase() === lcHeader) {
            requestHeaders[key] = newValue;
        }
    }
}

function dispatchError(xhr) {
    const errMessage = xhr.properties.error;
    requestErrorSteps(xhr, 'error', DOMException.create(xhr._globalObject, [errMessage, 'NetworkError']));

    if (xhr._ownerDocument) {
        const error = new Error(errMessage);
        error.type = 'XMLHttpRequest'; // TODO this should become "resource loading" when XHR goes through resource loader

        xhr._ownerDocument._defaultView._virtualConsole.emit('jsdomError', error);
    }
}

function validCORSHeaders(xhr, responseHeaders, flag, properties, origin) {
    const acaoStr = responseHeaders['access-control-allow-origin'];
    const acao = acaoStr ? acaoStr.trim() : null;
    if (acao !== '*' && acao !== origin) {
        properties.error = 'Cross origin ' + origin + ' forbidden';
        dispatchError(xhr);
        return false;
    }
    const acacStr = responseHeaders['access-control-allow-credentials'];
    const acac = acacStr ? acacStr.trim() : null;
    if (flag.withCredentials && acac !== 'true') {
        properties.error = 'Credentials forbidden';
        dispatchError(xhr);
        return false;
    }
    return true;
}

function validCORSPreflightHeaders(xhr, responseHeaders, flag, properties) {
    if (!validCORSHeaders(xhr, responseHeaders, flag, properties, properties.origin)) {
        return false;
    }
    const acahStr = responseHeaders['access-control-allow-headers'];
    const acah = new Set(acahStr ? acahStr.trim().toLowerCase().split(headerListSeparatorRegexp) : []);
    const forbiddenHeaders = Object.keys(flag.requestHeaders).filter(header => {
        const lcHeader = header.toLowerCase();
        return !simpleHeaders.has(lcHeader) && !acah.has(lcHeader);
    });
    if (forbiddenHeaders.length > 0) {
        properties.error = 'Headers ' + forbiddenHeaders + ' forbidden';
        dispatchError(xhr);
        return false;
    }
    return true;
}

function requestErrorSteps(xhr, event, exception) {
    const { flag, properties, upload } = xhr;

    xhr.readyState = READY_STATES.DONE;
    properties.send = false;

    setResponseToNetworkError(xhr);

    if (flag.synchronous) {
        throw exception;
    }

    fireAnEvent('readystatechange', xhr);

    if (!properties.uploadComplete) {
        properties.uploadComplete = true;

        if (properties.uploadListener) {
            fireAnEvent(event, upload, ProgressEvent, {
                loaded: 0,
                total: 0,
                lengthComputable: false
            });
            fireAnEvent('loadend', upload, ProgressEvent, {
                loaded: 0,
                total: 0,
                lengthComputable: false
            });
        }
    }

    fireAnEvent(event, xhr, ProgressEvent, {
        loaded: 0,
        total: 0,
        lengthComputable: false
    });
    fireAnEvent('loadend', xhr, ProgressEvent, {
        loaded: 0,
        total: 0,
        lengthComputable: false
    });
}

function setResponseToNetworkError(xhr) {
    const { properties } = xhr;
    properties.responseCache = properties.responseTextCache = properties.responseXMLCache = null;
    properties.responseHeaders = {};
    xhr.status = 0;
    xhr.statusText = '';
}

// return a "request" client object or an event emitter matching the same behaviour for unsupported protocols
// the callback should be called with a "request" response object or an event emitter matching the same behaviour too
async function createClient(xhr) {
    const { flag, properties } = xhr;
    const urlObj = new URL(flag.uri);
    const uri = urlObj.href;
    const ucMethod = flag.method.toUpperCase();

    const { requestManager } = flag;

    const requestHeaders = {};

    for (const header in flag.requestHeaders) {
        requestHeaders[header] = flag.requestHeaders[header];
    }

    if (getRequestHeader(flag.requestHeaders, 'referer') === null) {
        requestHeaders.Referer = flag.referrer;
    }
    if (getRequestHeader(flag.requestHeaders, 'accept') === null) {
        requestHeaders.Accept = '*/*';
    }

    const crossOrigin = flag.origin !== urlObj.origin;
    if (crossOrigin) {
        requestHeaders.Origin = flag.origin;
    }

    if (flag.auth) {
        const user = flag.auth.user || '';
        const pass = flag.auth.pass || '';
        requestHeaders.Authorization = 'Basic ' + Buffer.from(`${user}:${pass}`, 'utf8').toString('base64');
    }

    requestHeaders['Accept-Encoding'] = 'gzip, deflate, br';

    const { body } = flag;

    async function doRequestAsync(client) {
        const redirects = [];
        let options = {
            method: flag.method,
            url: uri
        };
        do {
            options.headers = Object.assign(Object.create(null), { ...requestHeaders, ...options.headers });
            options.assign = {
                xhr: 1
            };
            console.log(`XHR Request: ${options.url}`);
            const stream = await properties.browser.http(options);
            console.log(`XHR Stream: ${stream.url}`);
            client.url = stream.url;
            redirects.push(stream);
            const hasBody = body !== undefined && body !== null && body !== '' && !(stream.method === 'HEAD' || stream.method === 'GET');
            if (hasBody && getRequestHeader(options.headers, 'content-type') === null) {
                options.headers['Content-Type'] = 'text/plain;charset=UTF-8';
            }
            if (hasBody) {
                if (!flag.formData) {
                    stream.end(body);
                } else {
                    // TODO: Write form data here
                    console.warn('Not implemented: XMLHttpRequest FormData body');
                    stream.end();
                }
            } else {
                stream.end();
            }
            await utils.waitForResponse(stream);
            const redirectOptions = properties.browser.getRedirectOptions(stream);
            if (redirectOptions != null) {
                await utils.dumpReadableStream(stream);
                if (redirects.length < 30) {
                    redirectOptions.headers = Object.create(null);
                    client.emit('redirect', redirectOptions);
                    options = redirectOptions;
                    continue;
                }
                throw new Error('Maximum number of redirects reached: 30');
            }
            client.responseHeaders = stream.responseHeaders;
            client.responseRawHeaders = stream.responseRawHeaders;
            client.statusCode = stream.statusCode;
            client.statusMessage = stream.statusMessage;
            client.emit('response');
            let contentEncoding = 'identity';
            if ('content-encoding' in stream.responseHeaders) {
                if (Array.isArray(contentEncoding)) {
                    if (contentEncoding.length === 1) {
                        contentEncoding = contentEncoding[0];
                    } else {
                        contentEncoding = null;
                    }
                } else {
                    contentEncoding = stream.responseHeaders['content-encoding'];
                }
            }
            if (contentEncoding != null) {
                contentEncoding = contentEncoding.toLowerCase();
            }
            let decompressStream = null;
            switch (contentEncoding) {
                case 'gzip':
                case 'x-gzip':
                    decompressStream = zlib.createGunzip();
                    break;
                case 'deflate':
                    decompressStream = zlib.createInflate();
                    break;
                case 'br':
                    decompressStream = zlib.createBrotliDecompress();
                    break;
                case 'identity':
                    break;
                default: {
                    const error = new Error(`Unknown content-encoding algorithm: ${stream.responseHeaders['content-encoding']}`);
                    error.code = 'ERR_BROWSER_UNKNOWN_CONTENT_ENCODING';
                    error.contentEncoding = stream.responseHeaders['content-encoding'];
                    throw error;
                }
            }
            const onRawData = data => {
                client.emit('data', data);
            };
            const onResponseData = data => {
                client.emit('response.data', data);
            };
            const onEnd = () => {
                client.emit('end');
            };
            const onClose = () => {
                if (decompressStream != null) {
                    decompressStream.off('data', onResponseData).off('end', onEnd);
                }
                stream.off('data', onRawData).off('data', onResponseData).off('end', onEnd);
                client.emit('close');
            };
            if (decompressStream != null) {
                stream.on('data', onRawData);
                decompressStream.on('data', onResponseData).once('end', onEnd);
                stream.pipe(decompressStream);
            } else {
                stream.on('data', onRawData).on('data', onResponseData).once('end', onEnd);
            }
            stream.once('close', onClose);
            break;
        } while (true);
    }

    function doRequest() {
        const client = new EventEmitter();
        client.requestHeaders = requestHeaders;
        client.requestRawHeaders = utils.makeRawHeaders(requestHeaders);
        doRequestAsync(client).catch(error => {
            client.emit('error', error);
        });
        return client;
    }

    const nonSimpleHeaders = Object.keys(flag.requestHeaders).filter(header => !simpleHeaders.has(header.toLowerCase()));

    if (crossOrigin && (!simpleMethods.has(ucMethod) || nonSimpleHeaders.length > 0 || properties.uploadListener)) {
        const preflightRequestHeaders = [];
        for (const header in requestHeaders) {
            // the only existing request headers the cors spec allows on the preflight request are Origin and Referrer
            const lcHeader = header.toLowerCase();
            if (lcHeader === 'origin' || lcHeader === 'referer') {
                preflightRequestHeaders[header] = requestHeaders[header];
            }
        }

        preflightRequestHeaders['Access-Control-Request-Method'] = flag.method;
        if (nonSimpleHeaders.length > 0) {
            preflightRequestHeaders['Access-Control-Request-Headers'] = nonSimpleHeaders.join(', ');
        }

        preflightRequestHeaders['User-Agent'] = flag.userAgent;

        flag.preflight = true;

        const stream = await properties.browser.http({
            method: 'OPTIONS',
            url: uri,
            headers: preflightRequestHeaders
        });

        const onResponse = () => {
            if (removeRequest != null) {
                stream.off('close', removeRequest);
                removeRequest();
            }
            // don't send the real request if the preflight request returned an error
            if (stream.statusCode < 200 || stream.statusCode > 299) {
                utils.dumpReadableStream(stream).catch(error => void error).finally(() => {
                    defer.reject(new Error('Response for preflight has invalid HTTP status code ' + stream.statusCode));
                });
                return;
            }
            // don't send the real request if we aren't allowed to use the headers
            if (!validCORSPreflightHeaders(xhr, stream.responseHeaders, flag, properties)) {
                utils.dumpReadableStream(stream).catch(error => void error);
                setResponseToNetworkError(xhr);
                return;
            }
            defer.resolve(doRequest());
        };

        let removeRequest = null;

        stream.once('response', onResponse);
        if (requestManager) {
            const req = {
                abort() {
                    stream.off('response', onResponse).on('error', error => void error);
                    stream.abort();
                    properties.abortError = true;
                    xhr.abort();
                }
            };
            removeRequest = requestManager.remove.bind(requestManager, req);
            stream.on('close', removeRequest);
        }
        const defer = {};
        defer.promise = new Promise((resolve, reject) => {
            defer.resolve = resolve;
            defer.reject = reject;
        });
        return defer.promise;
    } else {
        return doRequest();
    }
}

exports.headerListSeparatorRegexp = headerListSeparatorRegexp;
exports.simpleHeaders = simpleHeaders;
exports.preflightHeaders = preflightHeaders;
exports.getRequestHeader = getRequestHeader;
exports.updateRequestHeader = updateRequestHeader;
exports.dispatchError = dispatchError;
exports.validCORSHeaders = validCORSHeaders;
exports.requestErrorSteps = requestErrorSteps;
exports.setResponseToNetworkError = setResponseToNetworkError;
exports.createClient = createClient;
