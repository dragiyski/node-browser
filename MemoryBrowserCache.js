(function () {
    'use strict';

    const { URL } = require('url');

    const symbols = {
        storage: Symbol('storage')
    };

    class MemoryBrowserCache {
        constructor() {
            this[symbols.storage] = Object.create(null);
        }

        store(url, options = {}) {
            url = new URL(url);
            // Hash is browser specific and not part of the HTTP protocol
            url.hash = '';
            // If username and passwords are present, this resource is not cacheable, but that's a job for the browser. We just remove those if the browser forgot.
            url.username = '';
            url.password = '';
            options = { ...options };
            if (options.date instanceof Date) {
                options.date = Number(options.date);
            }
            if (!isFinite(options.date)) {
                throw new TypeError('Expected option [date] to be valid date/timestamp (number)');
            }
            if (options.buffer != null && !Buffer.isBuffer(options.buffer)) {
                throw new TypeError('Expected option [buffer] to be null/undefined or Buffer');
            }
            options.date = Math.floor(options.date / 1000);
            if (options.etag != null && typeof options.etag !== 'string') {
                throw new TypeError('Expected option [etag] to be null/undefined or string');
            }
            if (url in this[symbols.storage]) {
                const cache = this[symbols.storage][url];
                if (options.etag != null && cache.etag !== options.etag) {
                    options.force = true;
                }
                if (cache.date >= options.date && !options.force) {
                    return false;
                }
            }
            if (options.expires instanceof Date) {
                options.expires = Number(options.expires);
            }
            if (options.expires != null && typeof options.expires !== 'number') {
                options.expires = parseInt(options.expires);
                if (!isFinite(options.expires)) {
                    throw new TypeError('Expected option [expires] to be date/timestamp (number) (or convertible)');
                }
            }
            if (options.expires != null) {
                options.expires = options.expires / 1000;
            }
            if (options.maxAge == null && typeof options.maxAge === 'number') {
                throw new TypeError('Expected option [maxAge] to be a number, if specified');
            }
            this[symbols.storage][url] = {
                buffer: options.buffer,
                date: options.date,
                etag: options.etag,
                expires: options.expires,
                maxAge: options.maxAge
            };
            return true;
        }

        find(url, now = null) {
            url = new URL(url);
            url.hash = '';
            if (now == null) {
                now = Date.now();
            }
            if (now instanceof Date) {
                now = Number(now);
            }
            if (!isFinite(now)) {
                throw new TypeError('Expected argument [now] to be valid date/timestamp (number)');
            }
            now = Math.floor(now / 1000);
            if (url in this[symbols.storage]) {
                const cache = this[symbols.storage][url];
                if (cache.expires != null && now > cache.expires) {
                    return void delete this[symbols.storage][url];
                }
                const result = {
                    buffer: cache.buffer,
                    date: new Date(cache.date * 1000),
                    age: now - cache.date,
                    etag: cache.etag,
                    maxAge: cache.maxAge
                };
                if (cache.expires != null) {
                    result.expires = new Date(cache.expires * 1000);
                }
                return result;
            }
        }

        free(url) {
            if (url in this[symbols.storage]) {
                return delete this[symbols.storage][url];
            }
            return false;
        }
    }

    MemoryBrowserCache.default = new MemoryBrowserCache();

    module.exports = MemoryBrowserCache;
})();
