(function () {
    'use strict';

    const assert = require('assert').strict;
    const { Readable, Writable } = require('stream');
    const { URL } = require('url');
    const { EventEmitter } = require('events');
    const util = require('util');
    const utils = require('./utils');
    const zlib = require('zlib');

    const { JSDOM, VirtualConsole } = require('../jsdom');
    const { CookieJar } = require('tough-cookie');
    const ResourceLoader = require('./ResourceLoader');
    const DomainCache = require('./DomainCache');
    const TLSSessionCache = require('./TLSSessionCache');
    const HttpPool = require('./HttpPool');
    const HttpStream = require('./HttpStream');

    const REGEXP_MIME_TYPE = /^([-\w.]+)\/([-\w.]+)(?:\+([-\w.]+))?$/;

    const symbols = {
        options: Symbol('options'),
        referrerPolicy: Symbol('referrerPolicy'),
        getCookieString: Symbol('getCookieString')
    };

    const defaultOptions = {
        userAgent: `Mozilla/5.0 (${process.platform}; ${process.arch}) NodeJS/${process.version}`,
        maxRedirects: 30,
        resouces: true
    };

    const defaultState = {
        domainCache: DomainCache.default,
        TLSSessionCache: TLSSessionCache.default,
        httpPool: HttpPool.default,
        resourceLoader: ResourceLoader.default
    };

    class Browser extends EventEmitter {
        constructor(state, options) {
            super();
            state = {
                ...defaultState,
                ...state
            };
            this.domainCache = state.domainCache;
            this.tlsSessionCache = state.tlsSessionCache;
            this.httpPool = state.httpPool;
            this.cookieJar = state.cookieJar;
            this.resourceLoader = state.resourceLoader;
            this[symbols.options] = { ...defaultOptions, ...options };
            this.browser = Symbol('browser');
        }

        get userAgent() {
            return this[symbols.options].userAgent;
        }

        /**
         * Set the User-Agent header as specified in the options of the browser.
         * @param requestHeaders The request headers to be updated
         * @returns {this}
         */
        setUserAgent(requestHeaders) {
            requestHeaders['User-Agent'] = this[symbols.options].userAgent;
            return this;
        }

        setCacheHeaders(requestHeaders) {
            if (!utils.hasTokenInHeaders(requestHeaders, 'Cache-Control', 'no-cache')) {
                utils.addHeader(requestHeaders, 'Cache-Control', 'no-cache');
            }
            if (!utils.hasTokenInHeaders(requestHeaders, 'Pragma', 'no-cache')) {
                utils.addHeader(requestHeaders, 'Pragma', 'no-cache');
            }
        }

        /**
         * Get the referrer policy set by Referrer-Policy for a window.
         * @param {object|null|undefined} window The window for the master request
         * @returns {string} The referrer policy
         */
        getReferrerPolicy(window = null) {
            if (window != null && symbols.referrerPolicy in window) {
                return window[symbols.referrerPolicy];
            }
            return 'no-referrer-when-downgrade';
        }

        /**
         * Update the referrer policy for a window. Only recognizable values are set.
         * @param window The window to be updated
         * @param responseHeaders The response headers received for the request.
         * @returns {this}
         */
        setReferrerPolicy(window, responseHeaders) {
            if ('referrer-policy' in responseHeaders) {
                let policy = responseHeaders['referrer-policy'];
                if (typeof policy === 'string') {
                    policy = policy.toLowerCase();
                    switch (policy) {
                        case 'no-referrer':
                        case 'no-referrer-when-downgrade':
                        case 'origin':
                        case 'origin-when-cross-origin':
                        case 'same-origin':
                        case 'strict-origin':
                        case 'strict-origin-when-cross-origin':
                        case 'unsafe-url':
                            window[symbols.referrerPolicy] = policy;
                            break;
                    }
                }
            }
            return this;
        }

        setAcceptLanguage(requestHeaders) {
            if (this[symbols.options].languages != null) {
                let languages = this[symbols.options].languages;
                if (Array.isArray(languages) && languages.length > 0) {
                    languages = languages.join(', ');
                } else if (typeof languages === 'object') {
                    languages = utils.httpWeightToken(languages, false);
                }
                if (typeof languages === 'string' && languages.length > 0) {
                    requestHeaders['Accept-Language'] = languages;
                }
            }
            return this;
        }

        /**
         * Update the "Referer" (Note: the typo here is embedded in the standard) header based on the referrer policy.
         *
         * Note: If use set the last parameter to a string, the default referrer-policy will be enacted: no-referrer-when-downgrade;
         *
         * Note: It is possible to give null/undefined as third parameter. In that case request headers are not modified. This is done to allow master request
         * to call setReferrer safely.
         * @param {object} requestHeaders The request headers to be updated.
         * @param {string|URL} targetUrl The target URL which will received the "Referer" header.
         * @param {object|Window|string|null|undefined} window Either a window object (whose window.location.href is used as source URL) or a source URL.
         * @returns {this}
         */
        setReferrer(requestHeaders, targetUrl, window = null) {
            let policy = 'no-referrer-when-downgrade';
            let sourceUrl = null;
            if (typeof window === 'string') {
                try {
                    sourceUrl = new URL(window);
                } catch (e) {
                    return this;
                }
            } else if (window != null && typeof window === 'object') {
                if (window.location && typeof window.location.href === 'string') {
                    sourceUrl = new URL(window.location.href);
                }
                policy = this.getReferrerPolicy(window);
                if (policy == null || policy === false) {
                    return this;
                }
            }
            if (sourceUrl == null) {
                return this;
            }
            targetUrl = new URL(targetUrl, sourceUrl);
            switch (policy) {
                case 'origin':
                    requestHeaders.referer = sourceUrl.origin;
                    break;
                case 'origin-when-cross-origin':
                    requestHeaders.referer = sourceUrl[sourceUrl.origin === targetUrl.origin ? 'href' : 'origin'];
                    break;
                case 'same-origin':
                    if (sourceUrl.origin === targetUrl.origin) {
                        requestHeaders.referer = sourceUrl.href;
                    }
                    break;
                case 'strict-origin':
                    if (!(sourceUrl.protocol === 'https:' && targetUrl.protocol === 'http:')) {
                        requestHeaders.referer = sourceUrl.origin;
                    }
                    break;
                case 'strict-origin-when-cross-origin':
                    if (sourceUrl.origin === targetUrl.origin) {
                        requestHeaders.referer = sourceUrl.href;
                    } else if (!(sourceUrl.protocol === 'https:' && targetUrl.protocol === 'http:')) {
                        requestHeaders.referer = sourceUrl.origin;
                    }
                    break;
                case 'unsafe-url':
                    requestHeaders.referer = sourceUrl.href;
                    break;
                case 'no-referrer':
                    break;
                case 'no-referrer-when-downgrade':
                default:
                    if (!(sourceUrl.protocol === 'https:' && targetUrl.protocol === 'http:')) {
                        requestHeaders.referer = sourceUrl.href;
                    }
            }
            return this;
        }

        /**
         * Load the cookies for specific URL into the request headers.
         * @param {object} requestHeaders The request headers to be updated;
         * @param {string|URL} targetUrl The URL that would received the cookies;
         * @param {Date|null} now The time of the request.
         * @returns {Promise<this>}
         */
        async loadCookies(requestHeaders, targetUrl, now = null) {
            if (now == null) {
                now = new Date();
            }
            const cookies = await this.cookieJar.getCookieString('' + targetUrl, {
                http: true,
                now,
                ignoreError: true
            });
            if (typeof cookies === 'string' && cookies.length > 0) {
                utils.addHeader(requestHeaders, 'Cookies', cookies);
            }
            return this;
        }

        /**
         * Save the cookies obtained by the response.
         * @param {object} responseHeaders The response headers returned from the server. Expected lowercase header properties.
         * @param {string|URL} targetUrl The url the cookies come from.
         * @param {Date|null} now The time of the request.
         * @returns {Promise<this>}
         */
        async saveCookies(responseHeaders, targetUrl, now = null) {
            if (now == null) {
                now = new Date();
            }
            if (!('set-cookie' in responseHeaders)) {
                return this;
            }
            let cookies = responseHeaders['set-cookie'];
            if (!Array.isArray(cookies)) {
                cookies = [cookies];
            }
            for (const cookie of cookies) {
                await this.cookieJar.setCookie(cookie, '' + targetUrl, {
                    http: true,
                    now,
                    ignoreError: true
                });
            }
            return this;
        }

        // TODO: Make separate methods:
        // TODO: async response(stream): void  - wait for stream response;
        // TODO: async follow(stream, [data]): stream - return new stream for a redirect response
        // TODO: async http(options): stream - make an HTTP request and return the stream. Event listeners could be attached to handle cookies. Additional request headers might be added.

        dataUrl({ url, ...options }) {
            let data = url.pathname.split(',');
            let decoder = stringDecode;
            let encoding = null;
            let mime = null;
            if (data.length > 1) {
                const header = data.shift().split(';').map(s => s.trim());
                data = data.join(',');
                mime = header.shift();
                if (!REGEXP_MIME_TYPE.test(mime)) {
                    mime = null;
                }
                while (header.length > 0) {
                    const parameter = header.shift();
                    const keyValue = /^([^=]+)(?:=(.*))?$/.exec(parameter);
                    if (keyValue != null) {
                        const key = keyValue[1].trim().toLowerCase();
                        const value = keyValue[2] != null ? keyValue[2].trim() : null;
                        if (key === 'base64') {
                            decoder = base64decode;
                        } else if (key === 'encoding') {
                            try {
                                // eslint-disable-next-line no-new
                                new TextDecoder(value);
                                encoding = value;
                            } catch (e) {
                                if (e.code !== 'ERR_ENCODING_NOT_SUPPORTED') {
                                    throw e;
                                }
                                encoding = null;
                            }
                        }
                    }
                }
            }

            if (mime == null) {
                encoding = null;
                mime = 'text/plain;charset=US-ASCII';
            } else if (encoding != null) {
                mime += ';encoding=' + encoding;
            }

            const stream = new HttpStream();
            const extension = {
                id: 1,
                requestHeaders: Object.create(null),
                requestRawHeaders: [],
                responseHeaders: Object.create(null),
                responseRawHeaders: [],
                method: 'GET',
                scheme: 'data',
                authority: '',
                path: '',
                url
            };

            if (options.assign != null) {
                Object.assign(stream, options.assign);
            }

            extension.responseHeaders['content-type'] = mime;
            extension.responseRawHeaders.push('content-type', mime);
            data = decoder(data, encoding);
            if (Buffer.isBuffer(data)) {
                extension.responseHeaders['content-length'] = data.length.toString(10);
                extension.responseRawHeaders.push('content-length', data.length.toString(10));
                const writable = new Writable({
                    write(chunk, encoding, callback) {
                        callback();
                    },
                    final(callback) {
                        callback();
                    }
                });
                const readable = new Readable({
                    read(size) {
                        const bytes = Math.min(this._data.length - this._offset, size);
                        if (bytes > 0) {
                            this.push(this._data.slice(this._offset, this._offset + bytes));
                        }
                        this._offset += bytes;
                        if (this._offset >= this._data.length) {
                            this.push(null);
                        }
                    }
                });
                readable._data = data;
                readable._offset = 0;
                stream.setWritable(writable);
                stream.setReadable(readable);
                Object.defineProperty(stream, 'abort', {
                    configurable: true,
                    writable: true,
                    value: function () {}
                });
                process.nextTick(() => {
                    writable.end();
                    process.nextTick(() => {
                        stream.statusCode = 200;
                        stream.statusMessage = 'OK';
                        stream.emit('response');
                    });
                });
            } else {
                process.nextTick(() => {
                    const error = new Error('net::ERR_INVALID_URL');
                    error.code = 'ERR_INVALID_URL';
                    stream.destroy(error);
                });
            }
            Object.assign(stream, extension);
            return stream;

            function stringDecode(string, encoding) {
                if (encoding == null) {
                    encoding = 'US-ASCII';
                }
                return Buffer.from(decodeURIComponent(string), encoding);
            }

            function base64decode(string) {
                if (!/^[A-Za-z0-9+/=]+$/.test(string)) {
                    return null;
                }
                return Buffer.from(string, 'base64');
            }
        }

        async http({ method, url, headers, ...options }) {
            method = method.toUpperCase();
            url = new URL(url);
            if (url.protocol !== 'http:' && url.protocol !== 'https:' && url.protocol !== 'data:') {
                const protocol = url.protocol.substr(0, url.protocol.length);
                const error = new Error(`Unsupported protocol: ${protocol}`);
                error.protocol = protocol;
                error.code = 'ERR_BROWSER_UNSUPPORTED_PROTOCOL';
                throw error;
            }
            if (url.protocol === 'data:') {
                return this.dataUrl({ url, ...options });
            }
            const now = new Date();
            this.setUserAgent(headers);
            this.setAcceptLanguage(headers);
            this.setCacheHeaders(headers);
            await this.loadCookies(headers, url, now);
            const stream = await this.httpPool.request({
                method,
                url,
                headers
            });
            if (options.assign != null) {
                Object.assign(stream, options.assign);
            }
            stream.once('response', () => {
                this.saveCookies(stream.responseHeaders, url, now).catch(error => {
                    console.error(error);
                });
            });
            return stream;
        }

        getRedirectOptions(stream) {
            if (stream.statusCode == null) {
                return null;
            }
            const options = {};
            if (stream.statusCode === 301 || stream.statusCode === 302 || stream.statusCode === 303) {
                if (stream.method === 'HEAD') {
                    options.method = 'HEAD';
                } else {
                    options.method = 'GET';
                }
            } else if (stream.statusCode === 307 || stream.statusCode === 308) {
                options.method = stream.method;
            }
            if ('location' in stream.responseHeaders) {
                try {
                    options.url = new URL(stream.responseHeaders.location, stream.url);
                } catch (e) {}
            }
            if (options.method == null || options.url == null) {
                return null;
            }
            return options;
        }

        async resource({ url, headers, window }) {
            const redirects = [];
            headers['Accept-Encoding'] = 'gzip, deflate, br';
            let options = {
                method: 'GET',
                url
            };
            do {
                options.headers = Object.assign(Object.create(null), headers);
                this.setUserAgent(headers);
                this.setAcceptLanguage(headers);
                this.setCacheHeaders(headers);
                this.setReferrer(headers, url, window);
                const stream = await this.http(options);
                assert(!stream.destroyed);
                redirects.push(stream);
                try {
                    await utils.waitForResponse(stream);
                } catch (e) {
                    this.emit('resource.error', stream, e);
                    return null;
                }
                const redirectOptions = this.getRedirectOptions(stream);
                if (redirectOptions != null) {
                    if (redirects.length < this[symbols.options].maxRedirects) {
                        options = redirectOptions;
                        try {
                            await utils.dumpReadableStream(stream);
                        } catch (e) {
                            this.emit('resource.error', stream, e);
                            return null;
                        }
                        continue;
                    } else {
                        this.emit('redirect.loop', redirects);
                        return null;
                    }
                }
                assert(!stream.destroyed);
                return stream;
            } while (true);
        }

        async page(targetUrl) {
            targetUrl = new URL(targetUrl);
            const redirects = [];
            let options = {
                method: 'GET',
                url: targetUrl
            };
            do {
                options.headers = Object.create(null);
                options.headers['Sec-Fetch-Mode'] = 'navigate';
                options.headers['Sec-Fetch-Dest'] = 'document';
                options.headers['Sec-Fetch-Site'] = 'none';
                options.headers['Sec-Fetch-User'] = '?1';
                options.headers.Accept = utils.httpWeightToken({
                    'text/html': true,
                    'application/xhtml+xml': true,
                    'application/xml': 0.9,
                    '*/*': 0.1
                });
                options.headers['Accept-Encoding'] = 'gzip, deflate, br';
                const stream = await this.http(options);
                redirects.push(stream);
                await utils.waitForResponse(stream);
                const redirectOptions = this.getRedirectOptions(stream);
                if (redirectOptions != null) {
                    if (redirects.length < this[symbols.options].maxRedirects) {
                        await utils.dumpReadableStream(stream);
                        options = redirectOptions;
                        continue;
                    }
                    this.emit('redirect.loop', redirects);
                }
                const data = await utils.readResponse(stream);
                return this.createWindow(stream, data);
            } while (true);
        }

        createWindow(stream, data) {
            let contentType = stream.responseHeaders['content-type'];
            if (Array.isArray(contentType)) {
                if (contentType.length === 1) {
                    contentType = contentType[0];
                } else {
                    contentType = null;
                }
            }
            if (typeof contentType !== 'string') {
                contentType = null;
            }
            if (contentType != null) {
                contentType = contentType.toLowerCase();
                const mimeType = contentType.split(';')[0].trim();
                if (mimeType !== 'text/html' && mimeType !== 'application/xml' && !mimeType.endsWith('+xml')) {
                    contentType = null;
                }
            }
            if (contentType == null) {
                const error = new Error('The response is not an HTML/XML web page');
                error.code = 'ERR_NOT_WEB_PAGE';
                error.stream = stream;
                error.data = data;
                throw error;
            }
            const virtualConsole = new VirtualConsole();
            const jsdomOptions = {
                url: stream.url.href,
                contentType,
                pretendToBeVisual: true,
                resources: this.resourceLoader,
                virtualConsole,
                browser: this,
                runScripts: 'dangerously',
                beforeParse: window => {
                    window[this.browser] = this;
                    window[Browser.symbol] = this;
                    window.document.domain = stream.url.hostname;
                    for (const event of ['dir', 'error', 'jsdomError', 'info', 'warn']) {
                        virtualConsole.on(event, (...args) => {
                            this.onConsole(event, window, ...args);
                        });
                    }
                    this.setReferrerPolicy(window, stream.responseHeaders);
                }
            };
            this.emit('resource', stream, data);
            return (new JSDOM(data, jsdomOptions)).window;
        }

        onConsole(type, ...args) {
            this.emit(`console.${type}`, ...args);
        }
    }

    Browser.symbol = Symbol('browser');
    Browser.redirect = Symbol('redirect');

    defaultState.cookieJar = Browser.defaultCookieJar = new CookieJar();

    module.exports = Browser;
})();
