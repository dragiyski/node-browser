(function () {
    'use strict';

    const assert = require('assert');
    const { EventEmitter } = require('events');
    const http = require('http');
    const http2 = require('http2');
    const net = require('net');
    const { performance } = require('perf_hooks');
    const stream = require('stream');
    const tls = require('tls');
    const { URL } = require('url');

    const utils = require('./utils');
    const DomainCache = require('./DomainCache');

    const HttpStream = require('./HttpStream');
    const TLSSessionCache = require('./TLSSessionCache');
    const helper = require('./helper');

    const methods = {
        connect: Symbol('connect'),
        close: Symbol('close'),
        getConnectionTarget: Symbol('getConnectionTarget'),
        makeTimeoutError: Symbol('makeTimeoutError'),
        createHttpStream: Symbol('createHttpStream'),
        createHttp2Stream: Symbol('createHttp2Stream'),
        http2Connect: Symbol('http2Connect'),
        idle: Symbol('idle'),
        onConnect: Symbol('onConnect'),
        onConnectError: Symbol('onConnectError'),
        onConnectTimeout: Symbol('onConnectTimeout'),
        onHttp2Close: Symbol('onHttp2Close'),
        onHttp2Connect: Symbol('onHttp2Connect'),
        onHttp2Error: Symbol('onHttp2Error'),
        onHttp2LocalSettings: Symbol('onHttp2LocalSettings'),
        onHttp2RemoteSettings: Symbol('onHttp2RemoteSettings'),
        onHttp2Timeout: Symbol('onHttp2Timeout'),
        onLookup: Symbol('onLookup'),
        onMaybeAvailable: Symbol('onMaybeAvailable'),
        onSecureConnect: Symbol('onSecureConnect'),
        onSecureError: Symbol('onSecureError'),
        onSecureTimeout: Symbol('onSecureTimeout'),
        onSocketConnect: Symbol('onSocketConnect'),
        onSocketClose: Symbol('onSocketClose'),
        onTlsSession: Symbol('onTlsSession'),
        onError: Symbol('onError'),
        removeHttpStream: Symbol('removeHttpStream'),
        removeHttp2Stream: Symbol('removeHttp2Stream'),
        secureConnect: Symbol('secureConnect'),
        start: Symbol('start')
    };

    const defaultOptions = {
        connectTimeout: 30000,
        tlsTimeout: 10000,
        http2Timeout: 10000,
        idleTimeout: 60000,
        goawayTimeout: 10000,
        maxConcurrentStreams: 10,
        h2: true
    };

    Object.setPrototypeOf(methods, null);
    Object.setPrototypeOf(defaultOptions, null);

    function optionOrigin(origin) {
        origin = new URL(origin);
        origin.pathname = '';
        origin.search = '';
        origin.hash = '';
        origin.slashes = true;
        return origin;
    }

    class HttpTransport extends EventEmitter {
        constructor(origin, options = {}) {
            super();
            this[HttpTransport.symbol] = {
                options: { ...defaultOptions, ...options },
                tlsSessionCache: TLSSessionCache.default,
                domainCache: DomainCache.default,
                origin: optionOrigin(origin),
                time: Object.create(null),
                socket: null,
                timer: null,
                activeSet: new Map()
            };
            for (const methodName in methods) {
                if (/^on[A-Z]/.test(methodName)) {
                    this[methods[methodName]] = this[methods[methodName]].bind(this);
                }
            }
            {
                const options = this[HttpTransport.symbol].options;
                if (options.socket != null) {
                    if (!(options.socket instanceof stream.Duplex)) {
                        throw new TypeError('Invalid option [socket]: expected a duplex stream');
                    }
                    if (!options.socket.readable) {
                        throw new TypeError('Invalid option [socket]: duplex stream must be readable');
                    }
                    if (!options.socket.writable) {
                        throw new TypeError('Invalid option [socket]: duplex stream must be wriable');
                    }
                    if (options.socket.readableObjectMode || options.socket.writableObjectMode) {
                        throw new TypeError('Invalid option [socket]: duplex stream must NOT be in object mode (both readable and writable)');
                    }
                    this[HttpTransport.symbol].socket = options.socket;
                    delete options.socket;
                }
                if (typeof options.lookup === 'function') {
                    this.once('lookup', options.lookup);
                    delete options.lookup;
                }
            }
            process.nextTick(this[methods.start].bind(this));
        }

        get secure() {
            const context = this[HttpTransport.symbol];
            const secure = context.options.secure;
            if (secure === false) {
                return false;
            }
            if (secure === true) {
                return true;
            }
            if (secure != null) {
                return true;
            }
            return context.origin.protocol === 'https:';
        }

        get origin() {
            return this[HttpTransport.symbol].origin.origin;
        }

        get destroyed() {
            return Boolean(this[HttpTransport.symbol].destroyed);
        }

        get closed() {
            return Boolean(this[HttpTransport.symbol].closed);
        }

        time(name) {
            const context = this[HttpTransport.symbol];
            const { time } = context;
            if (name in time) {
                return time[name];
            }
            return null;
        }

        request({ method, url, headers, ...options }) {
            if (this.closed || this.destroyed) {
                const error = new Error(`Transport is closed and it will not accept new requests`);
                error.code = 'ERR_TRANSPORT_CLOSED';
                throw error;
            }
            if (!this.isAvailable()) {
                const error = new Error(`Transport is not available right now, wait for "available" event`);
                error.code = 'ERR_TRANSPORT_BUSY';
                throw error;
            }
            if (method == null) {
                method = 'GET';
            }
            if (typeof method !== 'string' || method.length <= 0) {
                throw new TypeError('Expected option "method" to be non-empty string, if specified');
            }
            url = new URL(url, this.origin);
            if (url.origin !== this.origin) {
                const error = new Error(`A request does not match the transport origin:\n - request origin: ${url.origin}\n - transport origin: ${this.origin}`);
                error.code = 'ERR_TRANSPORT_ORIGIN_MISMATCH';
                throw error;
            }
            if (Array.isArray(headers)) {
                headers = rawHeadersToObject(headers);
            } else if (typeof headers === 'object') {
                headers = processHeaders(headers);
            } else if (headers == null) {
                headers = Object.create(null);
            } else {
                throw new TypeError('Expected option "headers" to be Array, Object or null/undefined/unspecified');
            }
            let stream;
            const context = this[HttpTransport.symbol];
            const task = {
                method,
                url,
                headers,
                ...options
            };
            if (context.timer != null) {
                clearTimeout(context.timer);
                context.timer = null;
            }
            if (context.http2 != null) {
                stream = this[methods.createHttp2Stream](task);
            } else {
                stream = this[methods.createHttpStream](task);
            }
            context.activeSet.set(stream, task);
            return stream;
        }

        [methods.start]() {
            let socket = this[HttpTransport.symbol].socket;
            if (socket == null) {
                socket = this[HttpTransport.symbol].stream = new net.Socket({
                    allowHalfOpen: false
                });
                this[HttpTransport.symbol].socket = socket;
            } else {
                this[HttpTransport.symbol].stream = socket;
            }
            if (socket instanceof net.Socket) {
                if (socket.pending) {
                    if (!socket.connecting) {
                        this[methods.connect]();
                    }
                    socket.once('connect', this[methods.onSocketConnect]).once('error', this[methods.onConnectError]);
                    return;
                }
            }
            this[methods.onSocketConnect]();
        }

        [methods.getConnectionTarget]() {
            const connectOptions = {};
            const options = this[HttpTransport.symbol].options;
            const origin = this[HttpTransport.symbol].origin;
            if (typeof options.path === 'string' && options.path.length > 0) {
                connectOptions.path = options.path;
                return connectOptions;
            }
            if (typeof options.host === 'string' && options.host.length > 0) {
                connectOptions.host = options.host;
            } else {
                connectOptions.host = origin.hostname;
            }
            let port;
            if (options.port != null) {
                connectOptions.port = parseInt(port = options.port, 10);
            } else if (origin.port.length > 0) {
                connectOptions.port = parseInt(port = origin.port, 10);
            } else {
                switch (origin.protocol) {
                    case 'https:':
                        connectOptions.port = 443;
                        break;
                    case 'http:':
                        connectOptions.port = 80;
                        break;
                    default: {
                        throw errorInvalidProtocol(origin.protocol);
                    }
                }
            }
            let error = null;
            if (!isFinite(connectOptions.port)) {
                error = new TypeError(`Invalid port, expected valid integer, got ${origin.port}`);
            } else if (connectOptions.port <= 0 || connectOptions.port >= 65536) {
                error = new RangeError(`Invalid port, port number should be between 1 and 65535, got ${port}`);
            }
            if (error != null) {
                error.port = origin.port;
                error.code = 'ERR_SOCKET_BAD_PORT';
                throw error;
            }
            return connectOptions;
        }

        [methods.connect]() {
            const options = this[HttpTransport.symbol].options;
            const socket = this[HttpTransport.symbol].socket;
            const time = this[HttpTransport.symbol].time;
            const connectOptions = this[methods.getConnectionTarget]();
            connectOptions.lookup = this[methods.onLookup];
            socket.connect(connectOptions);
            time.connectStart = performance.timeOrigin + performance.now();
            if (options.connectTimeout > 0) {
                this[HttpTransport.symbol].timer = setTimeout(this[methods.onConnectTimeout], options.connectTimeout);
            }
        }

        [methods.onSocketConnect]() {
            const context = this[HttpTransport.symbol];
            const socket = context.socket;
            const time = context.time;
            time.connectEnd = performance.timeOrigin + performance.now();
            if (context.timer != null) {
                clearTimeout(context.timer);
                context.timer = null;
            }
            socket.removeListener('error', this[methods.onConnectError]);
            if (socket instanceof net.Socket) {
                for (const key of ['localAddress', 'localPort', 'remoteAddress', 'remotePort']) {
                    if (socket[key] != null) {
                        context[key] = socket[key];
                    }
                }
            }
            if (this.secure) {
                return void this[methods.secureConnect]();
            }
            this[methods.onConnect]();
        }

        [methods.secureConnect]() {
            const context = this[HttpTransport.symbol];
            const options = context.options;
            const socket = context.socket;
            const time = context.time;
            const secureOptions = { ...options.secure };
            if (!Array.isArray(secureOptions.ALPNProtocols)) {
                secureOptions.ALPNProtocols = [];
            }
            if (options.h2 && secureOptions.ALPNProtocols.indexOf('h2') < 0) {
                secureOptions.ALPNProtocols.unshift('h2');
            }
            if (secureOptions.ALPNProtocols.indexOf('http/1.1') < 0) {
                secureOptions.ALPNProtocols.push('http/1.1');
            }
            secureOptions.servername = context.origin.host;
            secureOptions.rejectUnauthorized = false;
            const session = context.tlsSessionCache.get(this.origin);
            if (Buffer.isBuffer(session)) {
                // Session (or ticket) is an encrypted buffer given by the server, allowing new connection to be established with fewer round-trips.
                secureOptions.session = session;
            }
            secureOptions.minVersion = 'TLSv1';
            secureOptions.socket = socket;
            context.socket = tls.connect(secureOptions, this[methods.onSecureConnect]).once('error', this[methods.onSecureError]);
            context.socket.on('session', this[methods.onTlsSession]);
            time.tlsHandshakeStart = performance.timeOrigin + performance.now();
            if (secureOptions.tlsTimeout > 0) {
                context.timer = setTimeout(this[methods.onSecureTimeout], secureOptions.tlsTimeout);
            }
        }

        [methods.onTlsSession](session) {
            const context = this[HttpTransport.symbol];
            context.tlsSessionCache.set(this.origin, session);
        }

        [methods.onSecureConnect]() {
            const context = this[HttpTransport.symbol];
            const options = context.options;
            const socket = context.socket;
            const time = context.time;
            socket.secureConnecting = false;
            time.tlsHandshakeEnd = performance.timeOrigin + performance.now();
            if (context.timer != null) {
                clearTimeout(context.timer);
                context.timer = null;
            }
            socket.removeListener('error', this[methods.onSecureError]);
            if (options.rejectUnauthorized !== false && !socket.authorized) {
                const event = {
                    authorized: socket.authorized,
                    authorizationError: socket.authorizationError,
                    certificate: socket.getPeerCertificate(true)
                };
                this.emit('tls.authorization', event);
                if (!event.authorized) {
                    const error = new Error(`TLS authorization failed: ${event.authorizationError}`);
                    error.code = 'ERR_TLS_UNAUTHORIZED';
                    error.authorizationError = event.authorizationError;
                    error.certificate = event.certificate;
                    return void socket.destroy(error);
                }
            }
            if (socket.alpnProtocol === 'h2') {
                this[methods.http2Connect]();
            } else {
                this[methods.onConnect]();
            }
        }

        [methods.http2Connect]() {
            const context = this[HttpTransport.symbol];
            const options = context.options;
            const time = context.time;
            const settings = { ...http2.getDefaultSettings(), ...{ enablePush: false }, ...options.h2 };
            context.http2 = http2.connect(context.origin, {
                createConnection: () => context.socket,
                settings
            });
            context.http2.once('localSettings', this[methods.onHttp2LocalSettings]);
            context.http2.once('remoteSettings', this[methods.onHttp2RemoteSettings]);
            context.http2.once('error', this[methods.onHttp2Error]);
            context.http2.once('close', this[methods.onHttp2Close]);
            time.http2Start = performance.timeOrigin + performance.now();
            if (options.http2Timeout > 0) {
                context.timer = setTimeout(this[methods.onHttp2Timeout], options.http2Timeout);
            }
        }

        [methods.onHttp2LocalSettings](settings) {
            const context = this[HttpTransport.symbol];
            context.http2LocalSettings = settings;
            if (context.http2RemoteSettings != null) {
                this[methods.onHttp2Connect]();
            }
        }

        [methods.onHttp2RemoteSettings](settings) {
            const context = this[HttpTransport.symbol];
            context.http2RemoteSettings = settings;
            if (context.http2LocalSettings != null) {
                this[methods.onHttp2Connect]();
            }
        }

        [methods.onHttp2Connect]() {
            const context = this[HttpTransport.symbol];
            const time = context.time;
            time.http2End = performance.timeOrigin + performance.now();
            if (context.timer != null) {
                clearTimeout(context.timer);
                context.timer = null;
            }
            this[methods.onConnect]();
        }

        [methods.onLookup](host, options, callback) {
            const context = this[HttpTransport.symbol];
            const time = context.time;
            (async () => {
                time.lookupStart = performance.timeOrigin + performance.now();
                this.emit('lookup', Object.create(null, {
                    host: {
                        enumerable: true,
                        get: () => host,
                        set: value => {
                            host = value;
                        }
                    }
                }));
                {
                    const promise = helper.tryConvertToPromise(host);
                    if (promise != null) {
                        host = await promise;
                    }
                }
                const family = net.isIP(host);
                if (family) {
                    return {
                        address: host,
                        family
                    };
                }
                this.emit('lookup.before', host);
                let ip = context.domainCache.lookup(host);
                {
                    const promise = helper.tryConvertToPromise(ip);
                    if (promise != null) {
                        ip = await promise;
                    }
                }
                this.emit('lookup.after', host, ip != null ? { ...ip } : null);
                return ip;
            })().then(result => {
                time.lookupEnd = performance.timeOrigin + performance.now();
                if (result == null) {
                    const error = new Error(`Unable to resolve address for host: ${host}`);
                    error.code = 'ENOTFOUND';
                    error.host = host;
                    throw error;
                }
                callback(null, result.address, result.family);
            }).catch(callback);
        }

        destroy(error) {
            const context = this[HttpTransport.symbol];
            if (context.destroyed) {
                return;
            }
            context.time.destroyed = performance.timeOrigin + performance.now();
            context.destroyed = true;
            const noop = () => {
            };
            for (const [stream] of context.activeSet) {
                // If no error listener in installed, NodeJS throws error.
                // Since streams are not authoritative for errors occurring on the transport, they should not throw.
                if (stream.listenerCount('error') <= 0) {
                    stream.once('error', noop);
                }
                stream.destroy(error);
            }
            context.activeSet.clear();
            if (context.http2 != null) {
                context.http2
                    .removeListener('error', this[methods.onHttp2Error])
                    .removeListener('localSettings', this[methods.onHttp2LocalSettings])
                    .removeListener('remoteSettings', this[methods.onHttp2RemoteSettings]);
                delete context.http2;
            }
            if (context.socket != null && !context.socket.destroyed) {
                // In case of timeout or user-initiated error, close the socket.
                // This works well in HTTP2 library as session handles closing of the socket.
                // No case of destroy, no GOAWAY should be emitted, as it needs to pass synchronously,
                // because it indicates something really unexpected happened and we should close immediately.
                context.socket.destroy();
                delete context.socket;
            }
            if (context.timer != null) {
                clearTimeout(context.timer);
                delete context.timer;
            }
            let job;
            if (error) {
                context.error = error;
                job = () => {
                    if (this.listenerCount('error') > 0) {
                        this.emit('error', error);
                    }
                    this.emit('close');
                };
            } else {
                job = () => {
                    this.emit('close');
                };
            }
            process.nextTick(job);
        }

        close(callback) {
            if (typeof callback === 'function') {
                if (this.closed) {
                    return void process.nextTick(callback);
                }
                this.once('close', callback);
            }
            const context = this[HttpTransport.symbol];
            context.closed = true;
            if (context.http2) {
                // Closing mechanics for HTTP/2 are a little different.
                // The session is linked to socket
                // The lifecycle is to prevent new streams from being created and once all current streams finish, the session will close.
                // This will also close the socket eventually.
                context.http2.close();
            } else {
                process.nextTick(this[methods.onMaybeAvailable]);
            }
        }

        [methods.makeTimeoutError](startKey) {
            const context = this[HttpTransport.symbol];
            const time = context.time;
            const duration = performance.timeOrigin + performance.now() - time[startKey];
            const seconds = duration / 1000;
            const error = new Error(`Timed out after ${seconds.toFixed(6)} seconds`);
            error.code = 'ETIMEDOUT';
            return error;
        }

        [methods.onConnectError](error) {
            const nextError = new Error(`Connection error: ${error.message}`);
            nextError.code = 'ERR_CONNECTION_ERROR';
            nextError.previous = error;
            this.destroy(error);
        }

        [methods.onConnectTimeout]() {
            const context = this[HttpTransport.symbol];
            if (context.timer != null) {
                clearTimeout(context.timer);
                context.timer = null;
            }
            this[methods.onConnectError](this[methods.makeTimeoutError]('connectStart'));
        }

        [methods.onSecureError](error) {
            const nextError = new Error(`Secure socket layer error: ${error.message}`);
            nextError.code = 'ERR_TLS_ERROR';
            nextError.previous = error;
            this.destroy(error);
        }

        [methods.onSecureTimeout]() {
            const context = this[HttpTransport.symbol];
            if (context.timer != null) {
                clearTimeout(context.timer);
                context.timer = null;
            }
            this[methods.onSecureError](this[methods.makeTimeoutError]('tlsHandshakeStart'));
        }

        [methods.onHttp2Error](error) {
            const nextError = new Error(`HTTP2 error: ${error.message}`);
            nextError.code = 'ERR_HTTP2_ERROR';
            nextError.previous = error;
            this.destroy(error);
        }

        [methods.onHttp2Timeout]() {
            const context = this[HttpTransport.symbol];
            if (context.timer != null) {
                clearTimeout(context.timer);
                context.timer = null;
            }
            this[methods.onHttp2Error](this[methods.makeTimeoutError]('http2Start'));
        }

        [methods.onError](error) {
            this.destroy(error);
        }

        [methods.onSocketClose]() {
            const context = this[HttpTransport.symbol];
            if (context.activeSet.size > 0) {
                const error = HttpStream.makeAbortError();
                this.destroy(error);
            } else {
                this.destroy();
            }
        }

        [methods.onHttp2Close]() {
            this[methods.onMaybeAvailable]();
        }

        [methods.close]() {
            // Transport is "idle" and "closed"
            const context = this[HttpTransport.symbol];
            if (context.http2) {
                // NodeJS's HTTP2 does its own thing on closing. It eventually emit "close" event we will be waiting for.
                return;
            }
            // For HTTP/1.1 we destroy the transport since we are idle.
            this.destroy();
        }

        [methods.onConnect]() {
            const context = this[HttpTransport.symbol];
            context.connected = true;
            if (context.http2 != null) {
                context.http2.once('error', this[methods.onError]);
            } else {
                context.socket.once('error', this[methods.onError]);
            }
            context.socket.once('close', this[methods.onSocketClose]);
            process.nextTick(this[methods.onMaybeAvailable]);
        }

        isAvailable() {
            const context = this[HttpTransport.symbol];
            if (context.closed) {
                return false;
            }
            if (context.socket == null) {
                return false;
            }
            if (context.socket.destroyed) {
                return false;
            }
            if (!context.connected) {
                return false;
            }
            const activeStreams = context.activeSet != null ? context.activeSet.size : 0;
            let maxStreams = 0;
            if (context.socket != null && !context.socket.destroyed && context.socket.readable && context.socket.writable) {
                maxStreams = 1;
            }
            if (context.http2 != null && !context.http2.closed && context.http2LocalSettings != null && context.http2RemoteSettings != null) {
                maxStreams = Math.min(context.http2LocalSettings.maxConcurrentStreams, context.http2RemoteSettings.maxConcurrentStreams, context.options.maxConcurrentStreams);
            }
            return activeStreams < maxStreams;
        }

        [methods.onMaybeAvailable]() {
            const context = this[HttpTransport.symbol];
            if (this.closed) {
                if (context.activeSet.size <= 0 && !this.destroyed) {
                    this.destroy();
                }
                return;
            }
            if (this.isAvailable()) {
                this.emit('available');
            }
            if (this.isAvailable() && context.activeSet.size <= 0) {
                this[methods.idle]();
            }
            if (this.closed && context.activeSet.size <= 0 && !this.destroyed) {
                this.destroy();
            }
        }

        [methods.idle]() {
            const context = this[HttpTransport.symbol];
            this.emit('idle');
            if (context.timer != null) {
                clearTimeout(context.timer);
                context.timer = null;
            }
            if (context.options.idleTimeout > 0) {
                context.timer = setTimeout(() => {
                    this.close();
                }, context.options.idleTimeout);
            }
        }

        [methods.createHttpStream]({ method, url, headers: requestHeaders, ...options }) {
            const context = this[HttpTransport.symbol];
            if (context.options.idleTimeout > 0) {
                addConnectionKeepAlive(requestHeaders, context.options.idleTimeout);
            } else {
                addConnectionClose(requestHeaders);
            }
            for (const key in requestHeaders) {
                const headerName = key.toLowerCase();
                if (headerName === 'host') {
                    delete requestHeaders[key];
                }
            }
            if (method.toUpperCase() === 'CONNECT') {
                const error = new Error('HTTP Method CONNECT should not be send through HttpTransport');
                error.code = 'ERR_HTTP_METHOD_CONNECT';
                throw error;
            }
            requestHeaders.Host = url.host;
            let path = url.pathname;
            if (url.search.startsWith('?')) {
                path += url.search;
            }
            if (context.origin.protocol === 'https:') {
                if (!context.socket.encrypted) {
                    console.error(new Error(`Fatal: Socket is not encrypted for url: ${url}`));
                    process.exit(1);
                }
            }
            const request = http.request({
                method,
                path,
                headers: requestHeaders,
                setHost: false,
                maxHeaderSize: 65536,
                createConnection: () => context.socket
            });
            const stream = new HttpStream();
            const extension = {
                id: 1,
                requestHeaders,
                requestRawHeaders: utils.makeRawHeaders(requestHeaders),
                httpVersion: '1.1',
                httpVersionMajor: 1,
                httpVersionMinor: 1,
                method,
                scheme: url.protocol,
                authority: url.host,
                path,
                url,
                transport: this
            };
            for (const key of ['remoteAddress', 'remotePort', 'localAddress', 'localPort']) {
                if (context[key] != null) {
                    extension[key] = context[key];
                }
            }
            Object.assign(stream, extension);
            stream.setWritable(request);
            const onResponse = response => {
                request.removeListener('error', onRequestError);
                stream.responseHeaders = response.headers;
                stream.responseRawHeaders = response.rawHeaders;
                stream.statusCode = response.statusCode;
                stream.statusMessage = response.statusMessage;
                stream.setReadable(response);
                stream.emit('response');
            };
            const onRequestError = () => {
                request.removeListener('response', onResponse);
            };
            const onAborted = () => {
                context.activeSet.delete(stream);
                stream.setWritable();
                process.nextTick(() => {
                    stream.emit('abort');
                    stream.destroy();
                });
                this.destroy();
            };
            const onClose = () => {
                request.removeListener('error', onRequestError);
                request.removeListener('response', onResponse);
                context.activeSet.delete(stream);
                process.nextTick(this[methods.onMaybeAvailable]);
            };

            function abort() {
                request.once('error', () => {}).abort();
            }

            Object.defineProperties(stream, {
                abort: {
                    configurable: true,
                    writable: true,
                    value: abort
                }
            });
            request.once('abort', onAborted).once('response', onResponse).once('error', onRequestError);
            stream.once('close', onClose);
            this.emit('stream', stream);
            return stream;
        }

        [methods.createHttp2Stream]({ method, url, headers: requestHeaders, ...options }) {
            const context = this[HttpTransport.symbol];
            let hasTrailers = false;
            for (const key in requestHeaders) {
                if (Object.hasOwnProperty.call(requestHeaders, key)) {
                    const headerName = key.toLowerCase();
                    // HTTP2 does not support connection-related headers
                    // Upgrade is not supported by HTTP2, there are other frames to create websockets
                    // Upgrade and HTTP2 when performing clear-text HTTP/1.1 to HTTP/2 transition should be handled by the transport, not the request
                    if (['connection', 'upgrade', 'host', 'http2-settings', 'proxy-connection', 'keep-alive'].indexOf(headerName) >= 0) {
                        delete requestHeaders[key];
                    } else if (headerName === 'te') {
                        // TE (really bad name for "Accept-Transfer-Encoding" is a request header suggesting to the server what kind of "transfer-encoding" is
                        // supported by this client. For HTTP2 the only acceptable value of this header is "trailers".
                        const te = Array.isArray(requestHeaders[key]) ? requestHeaders[key].join(',') : requestHeaders[key];
                        if (te.split(',').map(s => s.split(';')[0].trim().toLowerCase()).indexOf('trailers') >= 0) {
                            hasTrailers = key;
                        }
                        delete requestHeaders[key];
                    }
                }
            }
            if (hasTrailers) {
                requestHeaders[hasTrailers] = 'trailers';
            }
            if (method.toUpperCase() === 'CONNECT') {
                const error = new Error('HTTP Method CONNECT should not be send through HttpTransport');
                error.code = 'ERR_HTTP_METHOD_CONNECT';
                throw error;
            }
            const headers = Object.assign(Object.create(null), requestHeaders);
            headers[':method'] = method;
            headers[':scheme'] = url.protocol.substr(0, url.protocol.length - 1);
            headers[':authority'] = url.host;
            headers[':path'] = url.pathname;
            if (url.search.startsWith('?')) {
                headers[':path'] += url.search;
            }
            const http2Stream = context.http2.request(headers, {
                endStream: false
            });
            const stream = new HttpStream();
            const extension = {
                id: http2Stream.id,
                requestHeaders,
                // HTTP/2 does not provide raw headers, so we create it. Do note that those might be different from what actually sent.
                requestRawHeaders: utils.makeRawHeaders(requestHeaders),
                httpVersion: '2.0',
                httpVersionMajor: 2,
                httpVersionMinor: 0,
                method,
                scheme: requestHeaders[':scheme'],
                authority: requestHeaders[':authority'],
                path: requestHeaders[':path'],
                url,
                transport: this
            };
            for (const key of ['remoteAddress', 'remotePort', 'localAddress', 'localPort']) {
                if (context[key] != null) {
                    extension[key] = context[key];
                }
            }
            const responseHeaders = Object.create(null);
            const responseRawHeaders = [];
            // Note: "response" event is fired when the status is >= 200. For statuses <= 100 headers are not final, i.e it is possible to have:
            // HEADERS [100]
            // HEADERS [100]
            // HEADERS [100] +END_HEADERS
            // HEADERS [200]
            // HEADERS [200] +END_HEADERS
            // TODO: Setup listeners for 100-continue
            const onResponse = (headers, flags, rawHeaders) => {
                // Unlike HTTP/1.1, it is possible to have multiple "HEADERS" frames
                // We merge each header frame until END_HEADERS flag is set.
                mergeHeaderObjects(responseHeaders, headers);
                for (let i = 0; i < rawHeaders.length; i += 2) {
                    if (rawHeaders[i] === ':status') {
                        continue;
                    }
                    responseRawHeaders.push(rawHeaders[i], rawHeaders[i + 1]);
                }
                // END_HEADERS = 4
                if (flags & 4) {
                    // Once the header flag is set, stop listening for headers
                    http2Stream.off('response', onResponse);
                    // Extract the pseudo-header status and remove it from headers
                    stream.statusCode = parseInt(responseHeaders[':status']);
                    stream.statusMessage = http.STATUS_CODES[stream.statusCode];
                    delete responseHeaders[':status'];
                    stream.responseHeaders = responseHeaders;
                    stream.responseRawHeaders = responseRawHeaders;
                    // Emit a single response for all headers collected...
                    stream.emit('response');
                }
            };
            const onError = error => {
                stream.emit('error', error);
            };
            const onClose = () => {
                http2Stream.removeListener('response', onResponse).removeListener('error', onError);
                stream.removeListener('close', onClose);
                context.activeSet.delete(stream);
                process.nextTick(this[methods.onMaybeAvailable]);
            };
            Object.assign(stream, extension);
            stream.setWritable(http2Stream);
            stream.setReadable(http2Stream);

            function abort() {
                http2Stream.close(http2.constants.NGHTTP2_CANCEL);
            }

            Object.defineProperties(stream, {
                abort: {
                    configurable: true,
                    writable: true,
                    value: abort
                }
            });
            http2Stream.on('response', onResponse).on('error', onError);
            stream.on('close', onClose);
            this.emit('stream', stream);
            process.nextTick(this[methods.onMaybeAvailable]);
            return stream;
        }
    }

    function errorInvalidProtocol(protocol) {
        if (protocol.endsWith(':')) {
            protocol = protocol.substr(0, protocol.length - 1);
        }
        const error = new Error(`Unsupported protocol: ${protocol}`);
        error.protocol = protocol;
        error.code = 'ERR_HTTP_INVALID_PROTOCOL';
        return error;
    }

    function rawHeadersToObject(headerList) {
        if (headerList.length % 2 > 0) {
            throw new Error('Expected raw headers to be an array with even number of elements');
        }
        const headers = Object.create(null);
        for (let i = 0; i < headerList.length; i += 2) {
            const key = headerList[i];
            let value = headerList[i + 1];
            if (typeof key !== 'string' || key.length <= 0) {
                throw new Error(`Invalid header name at index ${i}, header names must be string`);
            }
            if (value == null) {
                continue;
            }
            if (typeof value === 'boolean') {
                value = Number(value);
            }
            if (typeof value === 'number') {
                if (!isFinite(value)) {
                    throw new Error(`Invalid header value at index ${i}, number values must be finite`);
                }
                value = value.toString(10);
            }
            if (typeof value !== 'string') {
                throw new Error(`Invalid header value at index ${i}, expected null, undefined, boolean, number, or string`);
            }
            if (key in headers) {
                if (!Array.isArray(headers[key])) {
                    headers[key] = [headers[key], value];
                } else {
                    headers[key].push(value);
                }
            } else {
                headers[key] = value;
            }
        }
        return headers;
    }

    function processHeaders(headerObject) {
        const headers = Object.create(null);
        for (const key in headerObject) {
            if (Object.hasOwnProperty.call(headerObject, key)) {
                let value = headerObject[key];
                if (Array.isArray(value)) {
                    value = value.map((value, index) => processHeaderValue(value, key, index));
                } else {
                    value = processHeaderValue(value, key);
                }
                if (value != null) {
                    headers[key] = value;
                }
            }
        }

        function processHeaderValue(value, key, index = null) {
            let id = key;
            if (index != null) {
                id = `${id}[${index}]`;
            }
            if (value == null) {
                return;
            }
            if (typeof value === 'boolean') {
                value = Number(value);
            }
            if (typeof value === 'number') {
                if (!isFinite(value)) {
                    throw new Error(`Invalid header value for ${id}: number values must be finite`);
                }
                value = value.toString(10);
            }
            if (typeof value !== 'string') {
                throw new Error(`Invalid header value for ${id}: expected null, undefined, boolean, number, or string`);
            }
            return value;
        }

        return headers;
    }

    function addHeader(headers, headerName, headerValue) {
        if (headerName in headers) {
            if (!Array.isArray(headers[headerName])) {
                headers[headerName] = [headers[headerName], headerValue];
            } else {
                headers[headerName].push(headerValue);
            }
        } else {
            headers[headerName] = headerValue;
        }
    }

    function addConnectionKeepAlive(headers, timeout) {
        let hasKeepAlive = false;
        let hasConnectionClose = false;
        let hasConnectionKeepAlive = false;
        for (const key in headers) {
            const headerName = key.toLowerCase();
            if (headerName === 'keep-alive') {
                hasKeepAlive = true;
            }
            if (headerName === 'connection') {
                const processValues = value => {
                    value = value.split(',').map(s => s.trim().split(';')[0].trim().toLowerCase());
                    if (value.indexOf('close') >= 0) {
                        hasConnectionClose = true;
                    }
                    if (value.indexOf('keep-alive') >= 0) {
                        hasConnectionKeepAlive = true;
                    }
                };
                if (Array.isArray(headers[key])) {
                    headers[key].forEach(processValues);
                } else {
                    processValues(headers[key]);
                }
            }
        }
        if (hasConnectionClose) {
            return;
        }
        if (!hasConnectionKeepAlive) {
            addHeader(headers, 'Connection', 'keep-alive');
        }
        if (!hasKeepAlive) {
            let timeoutString = Math.ceil(timeout / 1000);
            timeoutString = `timeout=${timeoutString.toFixed(0)}`;
            addHeader(headers, 'Keep-Alive', timeoutString);
        }
    }

    function addConnectionClose(headers) {
        const keepAliveKeys = [];
        let hasConnectionClose = false;
        let hasConnectionKeepAlive = false;
        for (const key in headers) {
            const headerName = key.toLowerCase();
            if (headerName === 'keep-alive') {
                keepAliveKeys.push(key);
            }
            if (headerName === 'connection') {
                const processValues = value => {
                    value = value.split(',').map(s => s.trim().split(';')[0].trim().toLowerCase());
                    if (value.indexOf('close') >= 0) {
                        hasConnectionClose = true;
                    }
                    if (value.indexOf('keep-alive') >= 0) {
                        hasConnectionKeepAlive = true;
                    }
                };
                if (Array.isArray(headers[key])) {
                    headers[key].forEach(processValues);
                } else {
                    processValues(headers[key]);
                }
            }
        }
        if (hasConnectionKeepAlive) {
            return;
        }
        if (!hasConnectionClose) {
            addHeader(headers, 'Connection', 'close');
        }
        for (const keepAliveKey of keepAliveKeys) {
            delete headers[keepAliveKey];
        }
    }

    function mergeHeaderObjects(target, source) {
        const addHeader = (name, value) => {
            if (name in target) {
                if (!Array.isArray(target[name])) {
                    target[name] = [target[name]];
                }
                target[name].push(value);
            } else {
                target[name] = value;
            }
        };
        for (const headerName in source) {
            if (Object.hasOwnProperty.call(source, headerName)) {
                if (Array.isArray(source[headerName])) {
                    source[headerName].forEach(value => addHeader(headerName, value));
                } else {
                    addHeader(headerName, source[headerName]);
                }
            }
        }
    }

    HttpTransport.symbol = Symbol('HttpTransport');

    module.exports = HttpTransport;
})();
