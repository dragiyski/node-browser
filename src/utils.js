(function () {
    'use strict';

    const { Writable } = require('stream');
    const zlib = require('zlib');

    /**
     * Used to convert thenable to promise. Returns undefined if the result is not Promise/Thenable.
     * @param object
     * @returns {Promise|undefined}
     */
    exports.tryConvertToPromise = function (object) {
        if (object == null) {
            return;
        }
        if (object instanceof Promise) {
            return object;
        }
        const then = object.then;
        if (typeof then === 'function') {
            return new Promise((resolve, reject) => {
                then.call(object, resolve, reject);
            });
        }
    };

    /**
     * Convert an object of key: <true/weight> number for Accept-* headers.
     *
     * For example:
     * {
     *     'text/html': true,
     *     'application/xml': 0.9,
     *     'image/png': 0.8,
     *     'image/*' 0.1
     * }
     * will result in a: text/html,application/xml;q=0.9,image/png;q=0.8,image/*;q=0.1
     *
     * Values that are not true or number are ignored. Number range is not checked, but it is recommended to be in the range [0-1). Value "true" means no "q="
     * parameter. The values without "q=" parameter are first, followed by values with "q=" parameter in descending order.
     * @param {object} object
     * @param {boolean} pretty
     * @returns {string}
     */
    exports.httpWeightToken = function (object, pretty = false) {
        const keys = Object.keys(object);
        keys.sort((ka, kb) => {
            const a = object[ka];
            const b = object[kb];
            if (a === true) {
                if (b === true) {
                    return 0;
                }
                return -1;
            }
            if (typeof a === 'number') {
                if (b === true) {
                    return 1;
                }
                if (typeof b === 'number') {
                    return b - a;
                }
                return -1;
            }
            if (b === true || typeof b === 'number') {
                return 1;
            }
            return 0;
        });
        const items = [];
        for (const key of keys) {
            if (object[key] === true) {
                items.push(key);
            } else if (typeof object[key] === 'number') {
                items.push(`${key};${pretty ? ' ' : ''}q=${object[key].toString(10)}`);
            }
        }
        return items.join(pretty ? ', ' : ',');
    };

    /**
     * Reads all data in a stream into buffer. It can operate in two different modes:
     * - If the length is not specified (i.e. it is unknown), grow the buffer on every incoming data (might be slow).
     * - If the length is known, allocate buffer for that length and never grow. Dump all the excessive data.
     * @param {ReadableStream} stream Readable stream to extract information from.
     * @param {number|null} length If known, the length expected to be read from that buffer.
     * @returns {Promise<Buffer>} A buffer containing all the information from that stream.
     */
    exports.loadReadableStream = async function (stream, length = null) {
        if (!stream.readable || stream.readableObjectMode) {
            throw new Error('Not a readable stream, or stream is in object mode');
        }
        if (length != null && (typeof length !== 'number' || length < 0 || length !== parseInt(length))) {
            throw new TypeError('Argument "length" is invalid, expected positive integer');
        }
        return new Promise((resolve, reject) => {
            let buffer = Buffer.allocUnsafe(length != null ? length : 0);
            let offset = 0;
            const onData = data => {
                if (length != null) {
                    const readLength = Math.min(length - offset, data.length);
                    data.copy(buffer, offset, 0, readLength);
                    offset += readLength;
                } else {
                    buffer = Buffer.concat([buffer, data]);
                }
            };
            const onEnd = () => {
                removeListeners();
                resolve(buffer);
            };
            const onError = error => {
                removeListeners();
                reject(error);
            };
            const removeListeners = () => {
                stream.removeListener('data', onData);
                stream.removeListener('end', onEnd);
                stream.removeListener('error', onError);
            };
            stream
                .on('data', onData)
                .once('end', onEnd)
                .once('error', onError);
        });
    };

    /**
     * Read the data from readable stream and throw it away. Useful for consumptions of HTTP bodies without destroying the stream. Especially in HTTP/1.1
     * transport, premature ending is only possible on closing the connection. If the same connection is used for multiple requests, the body must be read,
     * before we continue. This reads the body without memory overhead.
     * @param stream The stream whose data needs dumping.
     * @returns {Promise<Stream>} The same stream.
     */
    exports.dumpReadableStream = async function (stream) {
        if (stream.destroyed || stream.readableEnded) {
            return stream;
        }
        return new Promise((resolve, reject) => {
            const voidStream = new Writable({
                autoDestroy: true,
                write(chunk, encoding, callback) {
                    callback();
                },
                final(callback) {
                    callback();
                }
            });
            const onEnd = () => {
                removeListeners();
                resolve(stream);
            };
            const onError = error => {
                removeListeners();
                reject(error);
            };
            const removeListeners = () => {
                stream.removeListener('end', onEnd);
                stream.removeListener('error', onError);
            };
            stream.once('error', onError).once('end', onEnd).pipe(voidStream);
        });
    };

    /**
     * Wait for HttpStream to receive a response. It returns immediately (next tick due to async), if response is already received.
     * @param {HttpStream} stream The stream to wait for.
     * @returns {Promise<HttpStream>} The same stream.
     */
    exports.waitForResponse = async function (stream) {
        if (stream.responseHeaders != null) {
            return stream.responseHeaders;
        }
        return new Promise((resolve, reject) => {
            const onResponse = () => {
                removeListeners();
                resolve(stream.responseHeaders);
            };
            const onError = error => {
                removeListeners();
                reject(error);
            };
            const removeListeners = () => {
                stream.removeListener('response', onResponse);
                stream.removeListener('error', onError);
            };
            stream.once('response', onResponse).once('error', onError);
            if (!stream.writableEnded) {
                stream.end();
            }
        });
    };

    exports.parseIPv6 = function (ip) {
        ip = ip.split('::');
        let g = 0;
        for (let i = 0; i < ip.length; ++i) {
            ip[i] = ip[i].split(':');
            g += ip[i].length;
        }
        const ip4 = /([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/.exec(ip[ip.length - 1][ip[ip.length - 1].length - 1]);
        if (ip4) {
            ip[ip.length - 1].pop();
            ip[ip.length - 1].push(ip4[1] * 256 + (ip4[2] | 0));
            ip[ip.length - 1].push(ip4[3] * 256 + (ip4[4] | 0));
        }
        if (ip.length === 2 && g < 8) {
            if (ip[0].length === 1 && ip[0][0] === '') {
                ip[0].length = 0;
            }
            ip[2] = ip[1];
            ip[1] = [];
            while (g < 8) {
                ip[1].push(0);
                ++g;
            }
        }
        ip = Array.prototype.concat.apply([], ip);
        if (ip.length !== 8) {
            return null;
        }
        for (let i = 0; i < ip.length; ++i) {
            ip[i] = typeof ip[i] === 'number' ? ip[i] : ip[i].length > 0 ? parseInt(ip[i], 16) : 0;
            if (!isFinite(ip[i])) {
                return null;
            }
        }
        return ip;
    };

    exports.parseCacheControl = function (value) {
        value = value.split(',').map(s => s.trim());
        const result = Object.create(null);
        for (const item of value) {
            switch (item) {
                case 'must-revalidate':
                case 'no-cache':
                case 'no-store':
                case 'no-transform':
                case 'public':
                case 'private':
                case 'proxy-revalidate':
                    result[item] = true;
                    break;
                default: {
                    let key = item.split('=').map(s => s.trim());
                    let value;
                    if (key.length >= 2) {
                        value = key.slice(1).join('=');
                        key = key[0];
                    }
                    if (key === 'max-age') {
                        value = parseInt(value.trim(), 10);
                        if (isFinite(value)) {
                            result[key] = value;
                        }
                    }
                }
            }
        }
        return result;
    };

    exports.hasTokenInHttpValue = function (value, search) {
        const tokens = value.split(',').map(token => token.split(';')[0].trim().toLowerCase());
        return tokens.indexOf(search) >= 0;
    };

    exports.hasTokenInHeaders = function (headers, headerName, search) {
        headerName = headerName.toLowerCase();
        for (const key in headers) {
            const name = key.toLowerCase();
            if (name === headerName) {
                const value = headers[key];
                if (Array.isArray(value)) {
                    if (value.some(value => exports.hasTokenInHttpValue(value, search))) {
                        return true;
                    }
                } else {
                    if (exports.hasTokenInHttpValue(value, search)) {
                        return true;
                    }
                }
            }
        }
        return false;
    };

    exports.addHeader = function (headers, headerName, headerValue) {
        if (headerName in headers) {
            if (!Array.isArray(headerName)) {
                headers[headerName] = [headers[headerName], headerValue];
            } else {
                headers[headerName].push(headerValue);
            }
        } else {
            headers[headerName] = headerValue;
        }
    };

    exports.makeRawHeaders = function (headers) {
        const rawHeaders = [];
        for (const headerName in headers) {
            const headerValue = headers[headerName];
            if (Array.isArray(headerValue)) {
                for (const value of headerValue) {
                    rawHeaders.push(headerName, value);
                }
            } else {
                rawHeaders.push(headerName, headerValue);
            }
        }
        return rawHeaders;
    };

    exports.getDecompressor = function (encoding) {
        switch (encoding) {
            case 'gzip':
            case 'x-gzip':
                return zlib.createGunzip();
            case 'deflate':
                return zlib.createInflate();
            case 'br':
                return zlib.createBrotliDecompress();
        }
    };

    exports.readResponse = async function (stream) {
        if ('content-encoding' in stream.responseHeaders) {
            let contentEncoding = stream.responseHeaders['content-encoding'];
            if (Array.isArray(contentEncoding)) {
                contentEncoding = contentEncoding.join(',');
            }
            contentEncoding.toLowerCase();
            switch (contentEncoding) {
                case 'gzip':
                case 'x-gzip':
                case 'deflate':
                case 'br':
                    return exports.decompressStream(stream, exports.getDecompressor(contentEncoding));
                case 'identity':
                    break;
                default: {
                    const error = new Error(`Unknown content-encoding algorithm: ${contentEncoding}`);
                    error.code = 'ERR_BROWSER_UNKNOWN_CONTENT_ENCODING';
                    error.contentEncoding = stream.responseHeaders['content-encoding'];
                    throw error;
                }
            }
        }
        let contentLength = null;
        if ('content-length' in stream) {
            contentLength = parseInt(stream['content-length']);
            if (!isFinite(contentLength) || contentLength < 0) {
                contentLength = null;
            }
        }
        return exports.loadReadableStream(stream, contentLength);
    };

    exports.decompressStream = async function (stream, decompressor) {
        return new Promise((resolve, reject) => {
            let buffer = Buffer.allocUnsafe(0);
            let rawBuffer = Buffer.allocUnsafe(0);
            const onData = data => {
                buffer = Buffer.concat([buffer, data]);
            };
            const onRawData = data => {
                rawBuffer = Buffer.concat([rawBuffer, data]);
            };
            const onRawEnd = () => {
                stream.unpipe(decompressor).off('close', onClose);
                decompressor.end();
            };
            const onEnd = () => {
                stream.off('error', onStreamError).off('close', onClose);
                decompressor.off('error', onDecompressorError).off('data', onData);
                resolve(buffer);
            };
            const onStreamError = error => {
                console.error(`error: ${stream.url}`);
                console.error(error);
                stream.unpipe(decompressor).off('close', onClose);
                decompressor.off('error', onDecompressorError).off('data', onData).off('end', onEnd).on('error', () => {}).destroy();
                reject(error);
            };
            const onDecompressorError = error => {
                console.error(`error.unzip: ${stream.url}`);
                console.erorr(error);
                stream.off('error', onStreamError).off('close', onClose);
                decompressor.off('end', onEnd).off('data', onData);
                stream.unpipe(decompressor);
                decompressor.destroy();
                error.stream = stream;
                reject(error);
            };
            const onClose = () => {
                console.log(`close: ${stream.url}`);
                debugger;
            };
            stream.on('data', onRawData).once('close', onClose).once('error', onStreamError).once('end', onRawEnd);
            decompressor.on('data', onData).once('end', onEnd).once('error', onDecompressorError);
            stream.pipe(decompressor);
        });
    };
})();
