(function () {
    'use strict';

    const dns = require('dns');
    const events = require('events');
    const fs = require('fs');
    const net = require('net');
    const os = require('os');
    const { performance } = require('perf_hooks');
    const util = require('util');
    const utils = require('./helper');

    const symbols = {
        options: Symbol('options'),
        cache: Symbol('cache')
    };

    const methods = {
        lookup: Symbol('lookup'),
        resolveAddress: Symbol('resolveAddress')
    };

    const defaultOptions = {
        dnsTimeout: 15000,
        cacheLifetime: 3600000
    };

    class DomainCache extends events.EventEmitter {
        constructor(options = {}) {
            super();
            this[symbols.options] = { ...defaultOptions, ...options };
            this[symbols.cache] = Object.create(null);
        }

        lookup(host) {
            this.emit('lookup', Object.create(null, {
                host: {
                    enumerable: true,
                    get: () => host,
                    set: value => {
                        host = value;
                    }
                }
            }));
            {
                const promise = utils.tryConvertToPromise(host);
                if (promise != null) {
                    return promise.then(host => this[methods.lookup](host));
                }
            }
            if (host == null) {
                return null;
            }
            const family = net.isIP(host + '');
            if (family) {
                return host;
            }
            return this[methods.lookup](host);
        }

        saveToFile(filename, options = {}) {
            const object = this.serialize();
            const data = options.pretty ? JSON.stringify(object, null, 4) + os.EOL : JSON.stringify(object);
            return util.promisify(fs.writeFile)(filename, data, {
                encoding: 'utf8'
            });
        }

        saveToFileSync(filename, options = {}) {
            const object = this.serialize();
            const data = options.pretty ? JSON.stringify(object, null, 4) + os.EOL : JSON.stringify(object);
            return fs.writeFileSync(filename, data, {
                encoding: 'utf8'
            });
        }

        static async loadFromFile(filename, options = {}) {
            const data = await util.promisify(fs.readFile)(filename, { encoding: 'utf8' });
            return this.unserialize(data);
        }

        static loadFromFileSync(filename, options = {}) {
            const data = fs.readFileSync(filename, { encoding: 'utf8' });
            return this.unserialize(data);
        }

        toJSON() {
            return this.serialize();
        }

        [methods.lookup](host) {
            if (host in this[symbols.cache] && !(this[symbols.cache][host] instanceof Promise)) {
                if (this[symbols.options].cacheLifetime > 0 && this[symbols.cache][host].createdAt != null) {
                    const now = performance.timeOrigin + performance.now();
                    if (now - this[symbols.cache][host].createdAt > this[symbols.options].cacheLifetime) {
                        delete this[symbols.cache][host];
                    }
                }
            }
            if (host in this[symbols.cache]) {
                const addressList = this[symbols.cache][host];
                const synchronize = addressList => {
                    return addressList.length > 0 ? addressList[0] : null;
                };
                if (addressList instanceof Promise) {
                    return addressList.then(synchronize);
                }
                return synchronize(addressList);
            }
            this.emit('lookup.before', host);
            this[symbols.cache][host] = this[methods.resolveAddress](host);
            return this[symbols.cache][host].then(addressList => {
                this.emit('lookup.after', host, addressList.map(item => {
                    return { ...item };
                }));
                if (addressList.length <= 0) {
                    return null;
                }
                this[symbols.cache][host] = addressList;
                this[symbols.cache][host].createdAt = performance.timeOrigin + performance.now();
                return addressList[0];
            });
        }

        async [methods.resolveAddress](host) {
            return new Promise((resolve, reject) => {
                let timedOut = false;
                let timeout = null;
                if (this[symbols.options].dnsTimeout > 0) {
                    timeout = setTimeout(() => {
                        timedOut = true;
                        const duration = performance.timeOrigin + performance.now() - lookupStart;
                        const seconds = (duration / 1000).toFixed(6);
                        const error = new Error(`DNS lookup timed out after ${seconds} seconds:\n- type: A/AAAA\n- name: ${host}`);
                        error.host = host;
                        error.duration = duration;
                        error.code = 'ETIMEDOUT';
                        reject(error);
                    }, this[symbols.options].dnsTimeout);
                }
                const lookupStart = performance.timeOrigin + performance.now();
                dns.lookup(host, {
                    hints: dns.ADDRCONFIG,
                    all: true,
                    verbatim: true
                }, (error, addressList) => {
                    try {
                        if (timedOut) {
                            return;
                        }
                        if (timeout != null) {
                            clearTimeout(timeout);
                            timeout = null;
                        }
                        if (error) {
                            if (error.code === 'ENOTFOUND') {
                                return void resolve([]);
                            }
                            return void reject(error);
                        }
                        resolve(addressList);
                    } catch (e) {
                        reject(e);
                    }
                });
            });
        }

        serialize() {
            const save = {
                cache: this[symbols.cache],
                options: {}
            };
            if (isFinite(this[symbols.options].dnsTimeout)) {
                save.options.dnsTimeout = this[symbols.options].dnsTimeout > 0 ? this[symbols.options].dnsTimeout : 0;
            }
            if (isFinite(this[symbols.options].cacheLifetime)) {
                save.options.cacheLifetime = this[symbols.options].cacheLifetime > 0 ? this[symbols.options].cacheLifetime : 0;
            }
            return save;
        }

        static unserialize(data, options = {}) {
            data = JSON.parse(data);
            const object = new this({ ...data.options, ...options });
            if (data.cache != null && typeof data.cache === 'object') {
                Object.setPrototypeOf(data.cache, null);
                object[symbols.cache] = data.cache;
            }
            return object;
        }
    }

    DomainCache.default = new DomainCache();

    module.exports = DomainCache;
})();
