(function () {
    'use strict';

    const assert = require('assert');
    const { Duplex, Stream } = require('stream');

    const symbols = {
        readable: Symbol('readable'),
        readableState: Symbol('readableState'),
        writable: Symbol('writable'),
        writableState: Symbol('writableState')
    };
    const methods = {
        read: Symbol('read'),
        write: Symbol('write'),
        onReadable: Symbol('onReadable'),
        onReadableError: Symbol('onReadableError'),
        onReadableEnd: Symbol('onReadableEnd'),
        onReadableClose: Symbol('onReadableClose'),
        // onWritableDrain: Symbol('onWritableDrain'),
        onWritableError: Symbol('onWritableError'),
        // onWritableFinish: Symbol('onWritableFinish'),
        // onWritableClose: Symbol('onWritableClose'),
        // onWritableAborted: Symbol('onWritableAborted'),
        removeReadable: Symbol('removeReadable'),
        removeWritable: Symbol('removeWritable')
    };

    const defaultOptions = {
        readableHighWaterMark: 65536,
        writableHighWaterMark: 65536,
        defaultEncoding: 'utf8'
    };

    Object.setPrototypeOf(symbols, null);
    Object.setPrototypeOf(methods, null);
    Object.setPrototypeOf(defaultOptions, null);

    class HttpStream extends Duplex {
        constructor(options = {}) {
            options = { ...defaultOptions, ...options };
            super({
                autoDestroy: true,
                readableHighWaterMark: options.readableHighWaterMark,
                writableHighWaterMark: options.writableHighWaterMark,
                defaultEncoding: options.defaultEncoding,
                allowHalfOpen: true,
                readableObjectMode: false,
                writeableObjectMode: false,
                decodeStrings: true,
                emitClose: true
            });
            this[symbols.readableState] = {
                size: 0,
                readable: false,
                ended: false
            };
            this[symbols.writableState] = {
                task: null,
                ended: false
            };
            for (const methodName in methods) {
                if (/^on[A-Z]/.test(methodName)) {
                    this[methods[methodName]] = this[methods[methodName]].bind(this);
                }
            }
            // Errors in HTTP streams could result in process dying. We prevent that.
            this.on('error', () => {});
        }

        // abort() should not be included here. It should be created by the HttpTransport, since it is created differently for HTTP/1.1 and HTTP/2.0;
        // abort() for HTTP/1.1 calls request.abort(), which calls underlying socket.destroy(<no error>), which goes into HTTP parser, which destroys the
        // stream with an error "ECONNRESET" and message "socket hang up", meaning it expects more data (to be written), but the socket is abrptly closed.
        // Do note that calling abort() on stream carried by HTTP/1.1 will close the underlying transport. This is not a problem for HttpPool, as new connection
        // will be initiated for the waiting requests.
        // abort() for HTTP/2.0 calls http2stream.close(<error code>) which sends an RST_STREAM frame with an error code. The selected error code for abort is
        // 0x08 (CANCEL). This destroys the stream expecting no more frames from the server for that stream, but it does not close HTTP2 connection.

        getReadable() {
            return this[symbols.readable];
        }

        setReadable(stream) {
            if (stream == null) {
                if (this[symbols.readable] != null) {
                    this[methods.removeReadable]();
                }
                return this;
            }
            if (!(stream instanceof Stream)) {
                throw new TypeError('Invalid argument: expected stream.Stream instance');
            }
            if (!stream.readable) {
                throw new TypeError('Invalid stream: the stream is not readable');
            }
            if (stream.readableObjectMode) {
                throw new TypeError('Invalid stream: the stream is in object mode');
            }
            if (this[symbols.readable] != null) {
                this[methods.removeReadable]();
            }
            this[symbols.readable] = stream;
            this[symbols.readableState].ended = false;
            stream
                .once('error', this[methods.onReadableError])
                .once('end', this[methods.onReadableEnd])
                .once('close', this[methods.onReadableClose])
                .on('readable', this[methods.onReadable]);
            if (stream.readableEnded) {
                this[methods.onReadableEnd]();
            }
            return this;
        }

        _read(size) {
            const readableState = this[symbols.readableState];
            readableState.size += size;
            if (readableState.ended && !readableState.finished) {
                readableState.finished = true;
                this.push(null);
            }
            if (readableState.readable) {
                this[methods.read]();
            }
        }

        [methods.read]() {
            const stream = this[symbols.readable];
            const readableState = this[symbols.readableState];
            while (!readableState.finished && readableState.size > 0 && stream.readable) {
                const data = stream.read(readableState.size);
                if (data == null || data.length <= 0) {
                    // Wait for next "readable" event
                    readableState.readable = false;
                    break;
                }
                readableState.size -= data.length;
                if (!this.push(data)) {
                    // Wait for drain (next _read call), even if more data can be read
                    break;
                }
            }
        }

        [methods.onReadable]() {
            const readableState = this[symbols.readableState];
            readableState.readable = true;
            this[methods.read]();
        }

        [methods.onReadableError](error) {
            this[methods.removeReadable]();
            this.destroy(error);
        }

        [methods.onReadableEnd]() {
            const readableState = this[symbols.readableState];
            readableState.ended = true;
            readableState.readable = false;
            this[methods.removeReadable]();
            if (!readableState.finished) {
                readableState.finished = true;
                this.push(null);
            }
        }

        [methods.onReadableClose]() {
            // If everything is working correctly, this should never be called (when readable stream ends, listeners are removed);
            // However, if a connection ends abruptly, before the readable is registered as finished, this would be called.
            // Note: this is not the same as abort(). abort() closes the client-side of the stream and it is initiated by the client (whatever uses the
            // "node-browser" package). This will be called if socket is suddenly closed by the server, when it is not expected to do so.
            this[methods.removeReadable]();
            this.destroy(HttpStream.makeAbortError());
        }

        [methods.removeReadable]() {
            this[symbols.readable]
                .removeListener('error', this[methods.onReadableError])
                .removeListener('end', this[methods.onReadableEnd])
                .removeListener('close', this[methods.onReadableClose])
                .removeListener('readable', this[methods.onReadable]);
            this[symbols.readable] = null;
        }

        getWritable() {
            return this[symbols.writable];
        }

        setWritable(stream) {
            if (stream == null) {
                if (this[symbols.writable] != null) {
                    this[methods.removeWritable]();
                }
                return this;
            }
            if (!(stream instanceof Stream)) {
                throw new TypeError('Invalid argument: expected stream.Stream instance');
            }
            if (!stream.writable) {
                throw new TypeError('Invalid stream: the stream is not writable');
            }
            if (stream.writableObjectMode) {
                throw new TypeError('Invalid stream: the stream is in object mode');
            }
            if (this[symbols.writable] != null) {
                this[methods.removeWritable]();
            }
            this[symbols.writable] = stream;
            stream
                .once('error', this[methods.onWritableError]);
            const writableState = this[symbols.writableState];
            if (writableState.task != null) {
                this[methods.write]();
            }
            return this;
        }

        _write(data, encoding, callback) {
            const writableState = this[symbols.writableState];
            writableState.task = {
                data,
                callback
            };
            if (this[symbols.writable] != null) {
                this[methods.write]();
            }
        }

        _final(callback) {
            const writableState = this[symbols.writableState];
            writableState.task = {
                callback
            };
            if (this[symbols.writable] != null) {
                this[methods.write]();
            }
        }

        [methods.write]() {
            const writable = this[symbols.writable];
            const writableState = this[symbols.writableState];
            const task = writableState.task;
            if (task != null) {
                const { data, callback } = task;
                if (writable.writable && !writable.writableEnded && !writable.destroyed) {
                    if (Buffer.isBuffer(data)) {
                        writable.write(data, callback);
                    } else {
                        writable.end(error => {
                            this[methods.removeWritable]();
                            callback(error);
                        });
                    }
                } else {
                    callback(); // Ignore any write after end (unlike the NodeJS streams)
                }
            }
        }

        [methods.onWritableError](error) {
            this[methods.removeWritable]();
            this.destroy(error);
        }

        [methods.removeWritable]() {
            this[symbols.writable].removeListener('error', this[methods.onWritableError]);
            this[symbols.writable] = null;
        }

        static makeAbortError() {
            const error = new Error('socket hang up');
            error.code = 'ECONNRESET';
            return error;
        }
    }

    Object.defineProperties(HttpStream, {
        readable: {
            value: Symbol('readable')
        },
        writable: {
            value: Symbol('writable')
        }
    });

    module.exports = HttpStream;
})();
