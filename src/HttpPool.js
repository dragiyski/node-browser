(function () {
    'use strict';

    const { EventEmitter } = require('events');
    const { URL } = require('url');

    const HttpTransport = require('./HttpTransport');

    const methods = {
        createTransport: Symbol('createTransport'),
        emitClose: Symbol('emitClose'),
        onTransportAvailable: Symbol('onTransportAvailable'),
        onTransportIdle: Symbol('onTransportIdle'),
        onTransportClose: Symbol('onTransportClose'),
        onTransportError: Symbol('onTransportError'),
        onTransportLookup: Symbol('onTransportLookup'),
        onTransportLookupBefore: Symbol('onTransportLookupBefore'),
        onTransportLookupAfter: Symbol('onTransportLookupAfter'),
        onTransportStream: Symbol('onTransportStream')
    };

    const defaultOptions = {
        maxSockets: 10
    };

    class HttpPool extends EventEmitter {
        constructor(options) {
            super();
            this[HttpPool.symbol] = {
                queueSet: Object.create(null),
                activeSet: Object.create(null),
                idleSet: Object.create(null),
                requestQueue: [],
                options: { ...defaultOptions, ...options }
            };
            this[methods.emitClose] = this[methods.emitClose].bind(this);
        }

        // TODO: Setup close() method that should mark this pool as closed. Due to queue-ing it should not immediately close all transports,
        // TODO: but it should close() them, once they register as "idle" (close() will send GOAWAY in HTTP/2.0, destroy() will just FIN the socket).

        get closed() {
            return Boolean(this[HttpPool.symbol].closed);
        }

        async request({ method, url, headers, ...options }) {
            const context = this[HttpPool.symbol];
            if (context.closed) {
                const error = new Error('The pool is already closed');
                error.code = 'ERR_POOL_CLOSED';
                throw error;
            }
            let originInQueue = false;
            url = new URL(url);
            if (url.origin in context.activeSet) {
                if (context.activeSet[url.origin].isAvailable()) {
                    const transport = context.activeSet[url.origin];
                    delete context.idleSet[url.origin];
                    return transport.request({
                        method,
                        url,
                        headers,
                        ...options
                    });
                }
            } else if (context.options.maxSockets <= 0 || Object.keys(context.activeSet).length < context.options.maxSockets) {
                this[methods.createTransport](url.origin);
            } else {
                originInQueue = true;
            }
            if (!(url.origin in context.queueSet)) {
                context.queueSet[url.origin] = [];
            }
            const item = {
                method,
                url,
                headers,
                options
            };
            item.promise = new Promise((resolve, reject) => {
                item.resolve = resolve;
                item.reject = reject;
            });
            context.queueSet[url.origin].push(item);
            if (originInQueue) {
                context.requestQueue.push(item);
            }
            return item.promise;
        }

        [methods.onTransportAvailable](transport) {
            const context = this[HttpPool.symbol];
            if (transport.origin in context.queueSet) {
                const queue = context.queueSet[transport.origin];
                if (queue.length > 0 && transport.isAvailable()) {
                    const request = queue.shift();
                    const requestQueueIndex = context.requestQueue.indexOf(request);
                    if (requestQueueIndex >= 0) {
                        context.requestQueue.splice(requestQueueIndex, 1);
                    }
                    try {
                        const options = {};
                        options.method = request.method;
                        options.url = request.url;
                        options.headers = request.headers;
                        options.options = request.options;
                        delete context.idleSet[transport.origin];
                        const stream = transport.request(options);
                        request.resolve(stream);
                    } catch (error) {
                        request.reject(error);
                    }
                }
                if (queue.length <= 0) {
                    delete context.queueSet[transport.origin];
                }
            }
        }

        close(callback) {
            if (typeof callback === 'function') {
                if (this.closed) {
                    return void process.nextTick(callback);
                }
                this.once('close', callback);
            }
            const context = this[HttpPool.symbol];
            context.closed = true;
            // We do not proactively close() the transports, because we might have a requests in requestQueue.
            // Instead, when a transport becomes idle and there is no item to schedule from the requestQueue, we close the transport.
            if (Object.keys(context.activeSet) <= 0 && context.requestQueue.length <= 0) {
                process.nextTick(this[methods.emitClose]);
            }
        }

        [methods.onTransportIdle](transport) {
            const context = this[HttpPool.symbol];
            let nextInLine = null;
            for (let i = 0; i < context.requestQueue.length; ++i) {
                const item = context.requestQueue[i];
                if (item.url.origin === transport.origin) {
                    // Next in the queue is from this origin, so do not close it, yet.
                    return;
                }
                if (item.url.origin in context.activeSet) {
                    // Skip over requests that already has transports
                    continue;
                }
                // If we execute this, then there is a request with no transport in activeSet and not handled by the current transport.
                // This could only happen if we reached the maxSockets limit.
                nextInLine = item;
                break;
            }
            if (nextInLine != null || context.closed) {
                // Close the stream if:
                // 1. Some request from origin not represented in activeSet is waiting, but transport is not created (due to limits).
                // 2. This pool is closed.
                transport.close();
            } else {
                // If we are not going to schedule any job on that transport, then transport is fully idle.
                // Unless explicitly disabled, this would start a timer which will close the transport after certain amount of inactivity.
                // Timer is removed prematurely, if a new request is scheduled on that transport.
                context.idleSet[transport.origin] = transport;
                process.nextTick(() => {
                    if (!(transport.origin in context.idleSet)) {
                        // A lot can happen in one tick, so if this did not remain idle, we skip.
                        return;
                    }
                    this.emit('transport.idle', transport);
                    let active = 0;
                    for (const origin in context.activeSet) {
                        if (!(origin in context.idleSet)) {
                            ++active;
                            break;
                        }
                    }
                    if (!active) {
                        // If all transports are in the idleSet, report this as an idle.
                        // Note: This won't be emitted if an "transport.idle" added work to the transport.
                        this.emit('idle');
                    }
                });
            }
        }

        [methods.onTransportClose](transport) {
            process.nextTick(() => {
                this.emit('transport.close', transport);
            });
            const context = this[HttpPool.symbol];
            if (transport[HttpPool.symbol] != null) {
                transport.off('available', transport[HttpPool.symbol][methods.onTransportAvailable]);
                transport.off('idle', transport[HttpPool.symbol][methods.onTransportIdle]);
                transport.off('close', transport[HttpPool.symbol][methods.onTransportClose]);
            }
            if (context.activeSet[transport.origin] !== transport) {
                return;
            }
            if (transport.origin in context.queueSet) {
                return void this[methods.createTransport](transport.origin);
            } else {
                delete context.activeSet[transport.origin];
                delete context.idleSet[transport.origin];
            }
            if (context.options.maxSockets <= 0 || Object.keys(context.activeSet).length < context.options.maxSockets) {
                let nextInLine = null;
                for (let i = 0; i < context.requestQueue.length; ++i) {
                    const item = context.requestQueue[i];
                    if (item.origin in context.activeSet) {
                        continue;
                    }
                    nextInLine = item;
                }
                if (nextInLine != null) {
                    // The actual request (including the request queue) is handled in onTransportAvailable
                    this[methods.createTransport](nextInLine.url.origin);
                } else if (context.closed && Object.keys(context.activeSet).length <= 0) {
                    process.nextTick(this[methods.emitClose]);
                }
            }
        }

        [methods.onTransportError](transport, error) {
            this.emit('transport.error', transport, error);
        }

        [methods.onTransportLookup](transport, lookup) {
            this.emit('transport.error', transport, lookup);
        }

        [methods.onTransportLookupBefore](transport, host) {
            this.emit('transport.lookup.before', transport, host);
        }

        [methods.onTransportLookupAfter](transport, host, ip) {
            this.emit('transport.lookup.after', transport, host, ip);
        }

        [methods.onTransportStream](transport, stream) {
            this.emit('transport.stream', transport, stream);
        }

        [methods.createTransport](origin) {
            const context = this[HttpPool.symbol];
            const transport = context.activeSet[origin] = new HttpTransport(origin, context.transportOptions);
            transport[HttpPool.symbol] = {
                [methods.onTransportIdle]: this[methods.onTransportIdle].bind(this, transport),
                [methods.onTransportError]: this[methods.onTransportError].bind(this, transport),
                [methods.onTransportLookup]: this[methods.onTransportLookup].bind(this, transport),
                [methods.onTransportLookupBefore]: this[methods.onTransportLookupBefore].bind(this, transport),
                [methods.onTransportLookupAfter]: this[methods.onTransportLookupAfter].bind(this, transport),
                [methods.onTransportAvailable]: this[methods.onTransportAvailable].bind(this, transport),
                [methods.onTransportClose]: this[methods.onTransportClose].bind(this, transport),
                [methods.onTransportStream]: this[methods.onTransportStream].bind(this, transport)
            };
            transport
                .on('available', transport[HttpPool.symbol][methods.onTransportAvailable])
                .on('idle', transport[HttpPool.symbol][methods.onTransportIdle])
                .on('error', transport[HttpPool.symbol][methods.onTransportError])
                .on('lookup', transport[HttpPool.symbol][methods.onTransportLookup])
                .on('lookup.before', transport[HttpPool.symbol][methods.onTransportLookupBefore])
                .on('lookup.after', transport[HttpPool.symbol][methods.onTransportLookupAfter])
                .on('stream', transport[HttpPool.symbol][methods.onTransportStream])
                .once('close', transport[HttpPool.symbol][methods.onTransportClose]);
            this.emit('transport', transport);
        }

        [methods.emitClose]() {
            this.emit('close');
        }
    }

    HttpPool.symbol = Symbol('HttpPool');

    HttpPool.default = new HttpPool();

    module.exports = HttpPool;
})();
