(function () {
    'use strict';

    const fs = require('fs');
    const os = require('os');
    const { performance } = require('perf_hooks');
    const util = require('util');

    const symbols = {
        cache: Symbol('cache'),
        options: Symbol('options')
    };

    const defaultOptions = {
        cacheLifetime: 3600000
    };

    class TLSSessionCache {
        constructor(options = {}) {
            this[symbols.options] = { ...defaultOptions, ...options };
            this[symbols.cache] = Object.create(null);
        }

        set(origin, session) {
            if (Buffer.isBuffer(session)) {
                this[symbols.cache][origin] = {
                    createdAt: performance.timeOrigin + performance.now(),
                    session
                };
            } else {
                delete this[symbols.cache][origin];
            }
            return this;
        }

        get(origin) {
            if (origin in this[symbols.cache]) {
                if (this[symbols.options].cacheLifetime > 0 && isFinite(this[symbols.cache][origin].createdAt)) {
                    const now = performance.timeOrigin + performance.now();
                    if (now - this[symbols.cache][origin].createdAt > this[symbols.options].cacheLifetime) {
                        delete this[symbols.cache][origin];
                    }
                }
            }
            if (origin in this[symbols.cache]) {
                if (Buffer.isBuffer(this[symbols.cache][origin].session)) {
                    return this[symbols.cache][origin].session;
                }
                delete this[symbols.cache][origin];
            }
        }

        saveToFile(filename, options = {}) {
            const object = this.serialize();
            const data = options.pretty ? JSON.stringify(object, null, 4) + os.EOL : JSON.stringify(object);
            return util.promisify(fs.writeFile)(filename, data, {
                encoding: 'utf8'
            });
        }

        saveToFileSync(filename, options = {}) {
            const object = this.serialize();
            const data = options.pretty ? JSON.stringify(object, null, 4) + os.EOL : JSON.stringify(object);
            return fs.writeFileSync(filename, data, {
                encoding: 'utf8'
            });
        }

        static async loadFromFile(filename, options = {}) {
            const data = await util.promisify(fs.readFile)(filename, { encoding: 'utf8' });
            return this.unserialize(data);
        }

        static loadFromFileSync(filename, options = {}) {
            const data = fs.readFileSync(filename, { encoding: 'utf8' });
            return this.unserialize(data);
        }

        toJSON() {
            return this.serialize();
        }

        serialize() {
            const data = {
                options: {},
                cache: Object.create(null)
            };
            if (isFinite(this[symbols.options].cacheLifetime)) {
                if (this[symbols.options].cacheLifetime > 0) {
                    data.options.cacheLifetime = this[symbols.options].cacheLifetime;
                } else {
                    data.options.cacheLifetime = 0;
                }
            }
            for (const origin in this[symbols.cache]) {
                if (Buffer.isBuffer(this[symbols.cache][origin].session)) {
                    data.cache[origin] = {
                        session: this[symbols.cache][origin].session.toString('base64')
                    };
                    if (isFinite(this[symbols.cache][origin].createdAt)) {
                        data.cache[origin].createdAt = this[symbols.cache][origin].createdAt;
                    }
                }
            }
            return data;
        }

        static unserialize(data, options = {}) {
            data = JSON.parse(data);
            const object = new this({ ...data.options, ...options });
            if (data.cache != null && typeof data.cache === 'object') {
                for (const origin in data.cache) {
                    if (Object.hasOwnProperty.call(data.cache, origin) && data.cache[origin] != null && typeof data.cache[origin] === 'object' && typeof typeof data.cache[origin].session === 'string') {
                        object[symbols.cache][origin] = {
                            createdAt: data.cache[origin].createdAt,
                            session: Buffer.from(data.cache[origin].session, 'base64')
                        };
                    }
                }
            }
            return object;
        }
    }

    TLSSessionCache.default = new TLSSessionCache();

    module.exports = TLSSessionCache;
})();
