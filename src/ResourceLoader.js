(function () {
    'use strict';

    const assert = require('assert').strict;
    const { EventEmitter } = require('events');
    const net = require('net');
    // eslint-disable-next-line node/no-deprecated-api
    const punycode = require('punycode');

    const { ResourceLoader: JSDOMResourceLoader } = require('../jsdom');

    const utils = require('./utils');

    const symbols = {
        loader: Symbol('loader'),
        criteria: Symbol('criteria')
    };

    const methods = {
        fetch: Symbol('fetch'),
        isResourceAllowed: Symbol('isResourceAllowed')
    };

    class ResourceLoader extends EventEmitter {
        constructor(criteria = true) {
            super();
            this[symbols.criteria] = criteria;
        }

        fetch(url, options = {}) {
            url = new URL(url);
            const window = options.element.ownerDocument.defaultView.top;
            const element = options.element;
            const headers = Object.create(null);
            let accept = null;
            if (element.nodeName.toUpperCase() === 'LINK' && element.hasAttribute('rel') && element.getAttribute('rel').toLowerCase() === 'stylesheet') {
                if (!this[methods.isResourceAllowed](url, this[symbols.criteria].stylesheet, element)) {
                    return null;
                }
                if (options.element.hasAttribute('type')) {
                    const type = options.element.getAttribute('type').toLowerCase();
                    if (type !== 'text/css') {
                        return null;
                    }
                }
                accept = {
                    'text/css': true,
                    '*/*': 0.1
                };
                headers['Sec-Fetch-Dest'] = 'style';
            } else if (element.nodeName.toUpperCase() === 'SCRIPT') {
                if (!this[methods.isResourceAllowed](url, this[symbols.criteria].script, element)) {
                    return null;
                }
                accept = {
                    'application/ecmascript': true,
                    'application/javascript': true,
                    'text/javascript': true,
                    '*/*': 0.1
                };
                if (element.hasAttribute('type')) {
                    const type = element.getAttribute('type').toLowerCase();
                    if (type.length > 0 && type !== 'module') {
                        accept[type] = true;
                    }
                }
                headers['Sec-Fetch-Dest'] = 'script';
            } else if (['IFRAME', 'FRAME'].indexOf(element.nodeName.toUpperCase()) >= 0) {
                if (!this[methods.isResourceAllowed](url, this[symbols.criteria].frame, element)) {
                    return null;
                }
                accept = {
                    'text/html': true,
                    'application/xhtml+xml': true,
                    'application/xml': 0.9,
                    '*/*': 0.1
                };
                headers['Sec-Fetch-Dest'] = element.nodeName.toLowerCase();
            } else {
                return null;
            }
            headers.Accept = utils.httpWeightToken(accept);
            if (url.origin === window.location.origin) {
                headers['Sec-Fetch-Site'] = 'same-origin';
            } else {
                headers['Sec-Fetch-Site'] = 'cross-site';
            }
            const Browser = require('./Browser');
            const browser = window[Browser.symbol];
            let resourceStream = null;
            return browser.resource({
                url,
                headers,
                window
            }).then(stream => {
                resourceStream = stream;
                return utils.readResponse(stream);
            }).then(data => {
                browser.emit('resource', resourceStream, data);
                return data;
            });
        }

        [methods.isResourceAllowed](url, criteria, element) {
            if (criteria == null || criteria === true) {
                return true;
            }
            if (criteria === false) {
                return false;
            }
            if (Array.isArray(criteria)) {
                return criteria.some(criteria => this[methods.isResourceAllowed(url, criteria, element)]);
            }
            if (typeof criteria === 'function') {
                return criteria(url + '', element);
            }
            if (typeof criteria === 'object') {
                if (criteria instanceof RegExp) {
                    return criteria.test('' + url);
                }
                [
                    'protocol',
                    'hostname',
                    'port',
                    'host',
                    'username',
                    'password',
                    'origin',
                    'pathname',
                    'search',
                    'searchParams',
                    'hash',
                    'href'
                ].every(option => {
                    const optionHandler = criteria => {
                        if (typeof criteria === 'function') {
                            return criteria(url[option], element);
                        }
                        const value = '' + url[option];
                        if (criteria instanceof RegExp) {
                            return criteria.test(value);
                        }
                        if (typeof resourceMatcher[option] === 'function') {
                            return resourceMatcher[option].call(this, url, criteria, element);
                        }
                        if (typeof criteria === 'string') {
                            return criteria === value;
                        }
                        return true;
                    };
                    if (Array.isArray(criteria[option])) {
                        return criteria[option].some(optionHandler);
                    }
                    return optionHandler(criteria);
                });
            }
        }
    }

    const resourceMatcher = {
        hostname: function (url, criteria, element) {
            let value = url.hostname;
            // If either criteria or value is IP, we must match the IP, this could be either IPv4 or IPv6 or IPv6 encoded IPv4, i.e. ::ffff:127.0.0.1
            if (typeof criteria === 'string') {
                if (net.isIPv6(criteria)) {
                    const family = net.isIP(value);
                    if (!family) {
                        return false;
                    } else if (family === 4) {
                        value = `::ffff:${value}`;
                    }
                    value = utils.parseIPv6(value);
                    criteria = utils.parseIPv6(criteria);
                    for (let i = 0; i < 8; ++i) {
                        if (value[i] !== criteria[i]) {
                            return false;
                        }
                    }
                    return true;
                }
                if (net.isIPv4(criteria)) {
                    if (net.isIPv6(value)) {
                        criteria = `::ffff:${criteria}`;
                        value = utils.parseIPv6(value);
                        criteria = utils.parseIPv6(criteria);
                        for (let i = 0; i < 8; ++i) {
                            if (value[i] !== criteria[i]) {
                                return false;
                            }
                        }
                        return true;
                    } else if (!net.isIPv4(value)) {
                        return false;
                    }
                    return criteria === value;
                }
                // If both fields define hosts, criteria can be:
                // <host> - matches host exactly
                // *.<host> - matches host and all its subdomains
                // If <host> primary domain is @ (i.e. *.@, test.@), @ is replaced by the domain/subdomain of the document URL.
                // Example, a request for resource at mail.example.com to resource URL test.mail.example.com will match *.@, test.@, but not @ or other.@
                let haystack = criteria.toLowerCase().split('.').map(punycode.toASCII).reverse();
                const needle = value.toLowerCase().split('.').map(punycode.toASCII).reverse();
                if (haystack.length > 1 && haystack[0] === '@') {
                    haystack.shift();
                    haystack = element.ownerDocument.defaultView.top.location.hostname.toLowerCase()
                        .split('.')
                        .map(punycode.toASCII)
                        .reverse()
                        .concat(haystack);
                }
                for (let i = 0, hl = haystack.length; i < hl; ++i) {
                    if (haystack[i] === '*') {
                        break;
                    }
                    if (haystack[i] !== needle[i]) {
                        return false;
                    }
                }
            }
            return true;
        },
        port: function (url, criteria, element) {
            if (typeof criteria === 'string') {
                if (criteria.length <= 0) {
                    return url.port.length <= 0;
                }
                criteria = parseInt(criteria);
            }
            if (!isFinite(criteria)) {
                return false; // Port is never non-numeric string
            }
            if (criteria === 0) {
                return true;
            }
            if (criteria < 0 || criteria >= 65536) {
                return false;
            }
            let port = url.port;
            if (port.length <= 0) {
                if (url.protocol === 'http:') {
                    port = 80;
                } else if (url.protocol === 'https:') {
                    port = 443;
                } else {
                    port = -1;
                }
            } else {
                port = parseInt(port);
                if (!isFinite(port)) {
                    port = -1;
                }
            }
            return port === criteria;
        }
    };

    Object.defineProperty(JSDOMResourceLoader, Symbol.hasInstance, {
        configurable: true,
        writable: true,
        value: function (value) {
            value = Object.getPrototypeOf(value);
            while (value != null) {
                if (value === ResourceLoader.prototype || value === JSDOMResourceLoader.prototype) {
                    return true;
                }
                value = Object.getPrototypeOf(value);
            }
            return false;
        }
    });

    ResourceLoader.default = new ResourceLoader();

    module.exports = ResourceLoader;
})();
