(function () {
    'use strict';

    module.exports = {
        Browser: require('./src/Browser'),
        ResourceLoader: require('./src/ResourceLoader'),
        DomainCache: require('./src/DomainCache'),
        HttpPool: require('./src/HttpPool'),
        HttpStream: require('./src/HttpStream'),
        HttpTransport: require('./src/HttpTransport'),
        MemoryBrowserCache: require('./MemoryBrowserCache'),
        TLSSessionCache: require('./src/TLSSessionCache')
    };
})();
